package de.bixilon.pixlyzer

import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generators
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getGetter
import de.bixilon.pixlyzer.util.Util
import net.minecraft.Bootstrap
import net.minecraft.util.registry.Registry
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.FileWriter
import java.security.MessageDigest
import java.util.*
import java.util.zip.GZIPOutputStream


object PixLyzer {
    private val startTime = System.currentTimeMillis()

    val ENTITY_META_DATA_MAPPING = Util.readJsonResource("entities_metadata_mappings.json")


    private val GSON = GsonBuilder()
        .create()

    private val PRETTY_GSON = GsonBuilder()
        .setPrettyPrinting()
        .create()


    @JvmStatic
    fun main(args: Array<String>) {
        println("Starting PixLyzer")

        if (args.size != 4) {
            error("Usage: java -cp minecraft,pixlyzer de.bixilon.pixlyzer.PixLyzer <Output directory> <Hash directory> <Assets index> <Version id>")
        }

        val outputDirectory = File(args[0])
        outputDirectory.mkdirs()
        val hashDirectory = File(args[1])
        hashDirectory.mkdirs()
        val assetsIndex = File(args[2])
        val versionId = args[3]


        try {
            Class.forName("net.minecraft.DetectedVersion")
            throw  IllegalStateException("This branch only works with yarn mappings, not with mojangs!")
        } catch (exception: ClassNotFoundException) {
        }

        try {
            Class.forName("net.minecraft.MinecraftVersion")
        } catch (exception: ClassNotFoundException) {
            throw IllegalStateException("Can not find minecraft in the classpath. Please add it to the classpath and restart PixLyzer", exception)
        }



        println("Loading classes...")
        val classesLoadStartTime = System.currentTimeMillis()

        initializeGameVersionMethod?.invoke(null)

        Bootstrap.initialize()

        Util.forceClassInit(Registry::class.java)

        println("Class loading done in ${System.currentTimeMillis() - classesLoadStartTime}ms")


        val all = JsonObject()
        for (generator in Generators.GENERATORS) {
            val startTime = System.currentTimeMillis()
            println("Starting ${generator.name}...")

            try {
                generator.generate()
            } catch (exception: IllegalArgumentException) {
                // not implemented
                println("Skipping ${generator.name}: ${exception.message}")
                continue
            }
            if (generator.data.size() == 0) {
                if (generator.allowEmpty) {
                    continue
                } else {
                    error("${generator.name} has 0 entries!")
                }
            }
            generator.test()
            println("Tests succeeded for ${generator.name}")

            println("Saving to ${outputDirectory.absolutePath}/${generator.name}.json")

            writeJson("${outputDirectory.absolutePath}/${generator.name}", generator.data)
            all.add(generator.name, generator.data)


            println("Done generating ${generator.name} in ${System.currentTimeMillis() - startTime}ms, generated ${generator.entries} entries.")
        }

        println("Writing all to file...")

        writeJson("${outputDirectory.absolutePath}/all", all)

        val encodedOutput = GSON.toJson(all).byteInputStream()

        val tempFileOutputPath = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID().toString()

        val fileOutputStream = GZIPOutputStream(FileOutputStream(tempFileOutputPath))


        val buffer = ByteArray(4096)
        var length: Int
        while (encodedOutput.read(buffer, 0, buffer.size).also { length = it } != -1) {
            fileOutputStream.write(buffer, 0, length)
        }
        fileOutputStream.close()


        val fileInputStream = FileInputStream(tempFileOutputPath)

        val crypt = MessageDigest.getInstance("SHA-1")

        while (fileInputStream.read(buffer, 0, buffer.size).also { length = it } != -1) {
            crypt.update(buffer, 0, length)
        }
        fileInputStream.close()

        val hash: String = Util.byteArrayToHexString(crypt.digest())

        val outputFile = File(hashDirectory.absolutePath + "/" + hash.substring(0, 2) + "/" + hash + ".gz")
        outputFile.parentFile.mkdirs()

        if (outputFile.exists()) {
            println("Hash file does already exist, skipping")
        } else {
            File(tempFileOutputPath).renameTo(outputFile)
            println("Generated and saved hash file to ${outputFile.absolutePath}")
        }

        val assetsIndexJson = Util.readJsonFile(assetsIndex.absolutePath + ".json")

        assetsIndexJson.addProperty(versionId, hash)

        writeJson(assetsIndex.absolutePath, assetsIndexJson)

        println("Done in ${System.currentTimeMillis() - startTime}ms")
    }

    private fun writeJson(path: String, data: JsonObject) {
        val fileWriter = FileWriter("$path.json")
        fileWriter.write(PRETTY_GSON.toJson(data))
        fileWriter.close()

        val minFileWriter = FileWriter("$path.min.json")
        minFileWriter.write(GSON.toJson(data))
        minFileWriter.close()
    }


    private val initializeGameVersionMethod = getGetter(getClass("net.minecraft.SharedConstants"), "createGameVersion", "method_36208")
}
