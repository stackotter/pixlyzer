package de.bixilon.pixlyzer.generator.generators.enums

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.enchantment.EnchantmentTarget

object EnchantmentTargetsGenerator : Generator(
    "enchantment_targets"
) {
    override fun generate() {
        for (target in EnchantmentTarget.values()) {
            val enchantmentTargetData = JsonObject()

            enchantmentTargetData.addProperty("name", target.name)

            data.add(target.ordinal.toString(), enchantmentTargetData)

        }
    }
}
