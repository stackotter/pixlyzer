package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.Util
import net.minecraft.util.registry.Registry

object ParticleGenerator : Generator(
    "particles"
) {
    override fun generate() {
        for (particleType in Registry.PARTICLE_TYPE) {
            val resourceIdentifier = Registry.PARTICLE_TYPE.getId(particleType)
            val particleData = JsonObject()
            particleData.addProperty("id", Registry.PARTICLE_TYPE.getRawId(particleType))

            // load render model
            particleData.add("render", Util.readJsonMinecraftResource("assets/${resourceIdentifier!!.namespace}/particles/${resourceIdentifier.path}.json"))

            if (particleType.shouldAlwaysSpawn()) {
                particleData.addProperty("override_limiter", particleType.shouldAlwaysSpawn())
            }
            // ToDo type and meta data

            data.add(resourceIdentifier.toString(), particleData)
        }
    }
}
