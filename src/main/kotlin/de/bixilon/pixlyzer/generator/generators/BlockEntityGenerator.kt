package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.block.Block
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry

object BlockEntityGenerator : Generator(
    "block_entities"
) {
    override fun generate() {
        for ((blockEntityType, id, resourceIdentifier) in getBlockEntities()) {
            val blockEntityData = JsonObject()
            blockEntityData.addProperty("id", id)

            (BLOCK_ENTITY_VALID_BLOCKS_FIELD?.get(blockEntityType) as Set<Block>?)?.let {
                val blockTypes = JsonArray()

                for (block in it) {
                    blockTypes.add(Registry.BLOCK.getRawId(block))
                }

                if (blockTypes.size() > 0) {
                    blockEntityData.add("blocks", blockTypes)
                }
            }

            data.add(resourceIdentifier.toString(), blockEntityData)
        }
    }

    private val BLOCK_ENTITY_VALID_BLOCKS_FIELD = getField(BlockEntityType::class.java, "blocks")


    private fun getBlockEntities(): Set<Triple<BlockEntityType<*>, Int, Identifier>> {
        val registry = getField(Registry::class.java, "BLOCK_ENTITY_TYPE", "BLOCK_ENTITY")!!.get(null) as Registry<BlockEntityType<*>>

        val ret: MutableSet<Triple<BlockEntityType<*>, Int, Identifier>> = mutableSetOf()

        for (value in registry) {
            ret.add(Triple(value as BlockEntityType<*>, registry.getRawId(value), registry.getId(value)!!))
        }

        return ret
    }
}
