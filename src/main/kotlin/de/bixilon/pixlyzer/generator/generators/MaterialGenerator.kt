package de.bixilon.pixlyzer.generator.generators

import com.google.common.collect.HashBiMap
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.block.Material
import net.minecraft.util.Identifier
import java.lang.reflect.Modifier

object MaterialGenerator : Generator(
    "materials"
) {
    override fun generate() {
        for ((resourceLocation, material) in MATERIALS) {
            val materialData = JsonObject()
            val color = MATERIAL_GET_COLOR_METHOD.invoke(material)
            val colorColor = color::class.java.getDeclaredField("color").getInt(color)
            materialData.addProperty("color", colorColor)
            materialData.addProperty("push_reaction", material.pistonBehavior.name.toLowerCase())
            materialData.addProperty("blocks_motion", material.blocksMovement())
            materialData.addProperty("flammable", material.isBurnable)
            materialData.addProperty("liquid", material.isLiquid)
            materialData.addProperty("solid_blocking", material.blocksLight())
            materialData.addProperty("replaceable", material.isReplaceable)
            materialData.addProperty("solid", material.isSolid)


            data.add(resourceLocation.toString(), materialData)
        }
    }

    private val MATERIAL_GET_COLOR_METHOD = Material::class.java.getDeclaredMethod("getColor")


    private fun getMaterials(): HashBiMap<Identifier, Material> {
        val materials: HashBiMap<Identifier, Material> = HashBiMap.create()
        for (field in Material::class.java.declaredFields) {
            if (field.type != Material::class.java) {
                continue
            }
            if (!Modifier.isStatic(field.modifiers)) {
                continue
            }

            field.isAccessible = true
            materials[Identifier(field.name.toLowerCase())] = field.get(null) as Material
        }

        return materials
    }


    val MATERIALS = getMaterials()
}
