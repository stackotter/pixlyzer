package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.world.biome.Biome

object BiomeCategoryGenerator : Generator(
    "biome_categories"
) {
    override fun generate() {
        for (biomeCategory in Biome.Category.values()) {
            val biomeCategoryData = JsonObject()
            biomeCategoryData.addProperty("name", biomeCategory.name)

            data.add(biomeCategory.ordinal.toString(), biomeCategoryData)
        }
    }

}
