package de.bixilon.pixlyzer.generator.generators

import com.google.common.collect.ImmutableSet
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraft.sound.SoundEvent
import net.minecraft.util.registry.Registry
import net.minecraft.village.VillagerProfession

object VillagerProfessionGenerator : Generator(
    "villager_professions"
) {
    override fun generate() {
        for (villagerProfession in Registry.VILLAGER_PROFESSION) {
            val resourceIdentifier = Registry.VILLAGER_PROFESSION.getId(villagerProfession)
            val villagerProfessionData = JsonObject()
            villagerProfessionData.addProperty("id", Registry.VILLAGER_PROFESSION.getRawId(villagerProfession))

            PointOfInterestGenerator.POINT_OF_INTEREST_REGISTRY?.let {
                villagerProfessionData.addProperty("work_station", it.getRawId(WORK_STATION_VILLAGER_PROFESSION_TYPE_FIELD!!.get(villagerProfession)))
            }

            (GATHERABLE_ITEMS_FIELD?.get(villagerProfession) as ImmutableSet<Item>?)?.let {
                val requestedItems = JsonArray()
                for (item in it) {
                    requestedItems.add(Registry.ITEM.getRawId(item))
                }

                if (requestedItems.size() > 0) {
                    villagerProfessionData.add("requested_items", requestedItems)
                }
            }

            (SECONDARY_JOB_SITES_FIELD?.get(villagerProfession) as ImmutableSet<Block>?)?.let {
                val blocks = JsonArray()
                for (block in it) {
                    blocks.add(Registry.BLOCK.getRawId(block))
                }

                if (blocks.size() > 0) {
                    villagerProfessionData.add("blocks", blocks)
                }
            }

            (WORK_SOUND_PROFESSION?.get(villagerProfession) as SoundEvent?)?.let {
                villagerProfessionData.addProperty("work_sound", Registry.SOUND_EVENT.getRawId(it))
            }

            data.add(resourceIdentifier.toString(), villagerProfessionData)
        }
    }

    private val WORK_SOUND_PROFESSION = getField(VillagerProfession::class.java, "workSound")


    private val WORK_STATION_VILLAGER_PROFESSION_TYPE_FIELD = getField(VillagerProfession::class.java, "workStation")

    private val SECONDARY_JOB_SITES_FIELD = getField(VillagerProfession::class.java, "secondaryJobSites")
    private val GATHERABLE_ITEMS_FIELD = getField(VillagerProfession::class.java, "gatherableItems")
}
