package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.util.registry.Registry
import java.lang.reflect.Method

object PointOfInterestGenerator : Generator(
    "points_of_interest"
) {
    override fun generate() {
        if (POINT_OF_INTEREST_REGISTRY == null) {
            throw IllegalArgumentException("Not available in this version yet!")
        }

        for (pointOfInterestType in POINT_OF_INTEREST_REGISTRY) {
            val resourceIdentifier = POINT_OF_INTEREST_REGISTRY.getId(pointOfInterestType)!!
            val pointOfInterestData = JsonObject()
            pointOfInterestData.addProperty("id", POINT_OF_INTEREST_REGISTRY.getRawId(pointOfInterestType))
            // ToDo: pointOfInterestData.addProperty("ticket_count", pointOfInterestType.ticketCount)
            SEARCH_DISTANCE_METHOD?.invoke(pointOfInterestType)?.let {
                pointOfInterestData.addProperty("search_distance", it as Int)
            }

            (POINT_OF_INTEREST_MATCHING_STATES_FIELD?.get(pointOfInterestType) as Set<BlockState>?)?.let {
                val states = JsonArray()
                for (state in it) {
                    states.add(Block.getRawIdFromState(state))
                }
                pointOfInterestData.add("matching_states", states)
            }

            data.add(resourceIdentifier.toString(), pointOfInterestData)
        }
    }


    val POINT_OF_INTEREST_REGISTRY = getField(Registry::class.java, "POINT_OF_INTEREST_TYPE")?.get(null) as Registry<Any>?

    val POINT_OF_INTEREST_CLASS = getClass("net.minecraft.world.poi.PointOfInterestType", "net.minecraft.village.PointOfInterestType")

    private val POINT_OF_INTEREST_MATCHING_STATES_FIELD = getField(POINT_OF_INTEREST_CLASS, "blockStates")

    private val SEARCH_DISTANCE_METHOD: Method? = try {
        POINT_OF_INTEREST_CLASS?.getDeclaredMethod("method_21648")
    } catch (exception: Exception) {
        try {
            POINT_OF_INTEREST_CLASS?.getDeclaredMethod("getSearchDistance")
        } catch (exception: Exception) {
            null
        }
    }

}
