package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.stat.Stat
import net.minecraft.stat.StatFormatter
import net.minecraft.util.registry.Registry

object StatisticsGenerator : Generator(
    "statistics"
) {
    override fun generate() {
        for (statistic in Registry.STAT_TYPE) {
            val resourceIdentifier = Registry.STAT_TYPE.getId(statistic)
            val statisticData = JsonObject()
            statisticData.addProperty("id", Registry.STAT_TYPE.getRawId(statistic))

            statisticData.addProperty("translation_id", statistic.translationKey)

            statisticData.addProperty("unit", when (statistic.registry) {
                Registry.BLOCK -> "block"
                Registry.ITEM -> "item"
                Registry.ENTITY_TYPE -> "entity_type"
                Registry.CUSTOM_STAT -> "custom"
                StatFormatter.DECIMAL_FORMAT -> "decimal_format"
                StatFormatter.DEFAULT -> "default"
                StatFormatter.DIVIDE_BY_TEN -> "divide_by_ten"
                StatFormatter.DISTANCE -> "distance"
                StatFormatter.TIME -> "time"
                else -> TODO("Can not find unit ${statistic.registry}")
            })

            val sortedStatistics: MutableSet<String> = mutableSetOf()
            for (subStatistic in statistic) {
                val key = when (subStatistic) {
                    is Stat -> subStatistic.name
                    else -> subStatistic.toString()
                }
                sortedStatistics.add(key)
            }
            val subStatistics = JsonArray()
            for (subStatistic in sortedStatistics.toSortedSet()) {
                subStatistics.add(subStatistic)
            }
            if (subStatistics.size() > 0) {
                statisticData.add("sub_statistics", subStatistics)
            }

            data.add(resourceIdentifier.toString(), statisticData)
        }
    }
}
