package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.util.registry.Registry

object VillagerTypeGenerator : Generator(
    "villager_types"
) {
    override fun generate() {
        for (villagerType in Registry.VILLAGER_TYPE) {
            val resourceIdentifier = Registry.VILLAGER_TYPE.getId(villagerType)
            val villagerTypeData = JsonObject()
            villagerTypeData.addProperty("id", Registry.VILLAGER_TYPE.getRawId(villagerType))

            data.add(resourceIdentifier.toString(), villagerTypeData)
        }
    }
}
