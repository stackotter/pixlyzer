package de.bixilon.pixlyzer.generator.generators.enums

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.world.biome.Biome

object BiomePrecipitationsGenerator : Generator(
    "biome_precipitations"
) {
    override fun generate() {
        for (biomeCategory in Biome.Precipitation.values()) {
            val biomeCategoryData = JsonObject()
            biomeCategoryData.addProperty("name", biomeCategory.name)

            data.add(biomeCategory.ordinal.toString(), biomeCategoryData)
        }
    }

}
