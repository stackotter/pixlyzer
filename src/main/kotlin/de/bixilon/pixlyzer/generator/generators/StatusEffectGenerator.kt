package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.entity.attribute.EntityAttribute
import net.minecraft.entity.attribute.EntityAttributeModifier
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.entity.effect.StatusEffectType
import net.minecraft.util.registry.Registry
import java.lang.reflect.Field

object StatusEffectGenerator : Generator(
    "status_effects"
) {
    override fun generate() {
        for (mobEffect in STATUS_EFFECT_REGISTRY) {
            val resourceIdentifier = STATUS_EFFECT_REGISTRY.getId(mobEffect)
            val mobEffectData = JsonObject()
            mobEffectData.addProperty("id", STATUS_EFFECT_REGISTRY.getRawId(mobEffect))

            mobEffectData.addProperty("category", mobEffect.type.name.toLowerCase()) // ToDo: add category color
            mobEffectData.addProperty("color", mobEffect.color)

            mobEffect.translationKey?.let {
                mobEffectData.addProperty("translation_key", it)
            }
            val modifiers = JsonObject()

            for ((attribute, modifier) in mobEffect.getModifiers()) {
                val modifierData = JsonObject()
                modifierData.addProperty("name", modifier.name.toLowerCase())
                modifierData.addProperty("uuid", modifier.id.toString())
                modifierData.addProperty("amount", AMOUNT_ENTITY_ATTRIBUTE_MODIFIER_FIELD.getDouble(modifier))
                modifierData.addProperty("operation", modifier.operation.name.toLowerCase())

                modifiers.add(EntityGenerator.getKeyFromMobEffect(attribute), modifierData)
            }

            if (modifiers.size() > 0) {
                mobEffectData.add("attributes", modifiers)
            }

            data.add(resourceIdentifier.toString(), mobEffectData)
        }
    }

    private val MOB_EFFECT_CATEGORY_FIELD: Field = StatusEffect::class.java.getDeclaredField("type")
    private val MOB_EFFECT_MODIFIERS_FIELD: Field = StatusEffect::class.java.getDeclaredField("attributeModifiers")
    private val AMOUNT_ENTITY_ATTRIBUTE_MODIFIER_FIELD = getField(EntityAttributeModifier::class.java, "amount", "value")!!

    init {
        MOB_EFFECT_CATEGORY_FIELD.isAccessible = true
        MOB_EFFECT_MODIFIERS_FIELD.isAccessible = true
    }

    private fun StatusEffect.getCategory(): StatusEffectType {
        return MOB_EFFECT_CATEGORY_FIELD.get(this) as StatusEffectType
    }

    private fun StatusEffect.getModifiers(): Map<EntityAttribute, EntityAttributeModifier> {
        return MOB_EFFECT_MODIFIERS_FIELD.get(this) as Map<EntityAttribute, EntityAttributeModifier>
    }

    val STATUS_EFFECT_REGISTRY = getField(Registry::class.java, "STATUS_EFFECT", "MOB_EFFECT")!!.get(null) as Registry<StatusEffect>
}
