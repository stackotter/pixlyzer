package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.generator.generators.enums.CreativeInventoryTabGenerator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.getGetter
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.entity.EntityType
import net.minecraft.entity.EquipmentSlot
import net.minecraft.fluid.Fluid
import net.minecraft.item.*
import net.minecraft.sound.SoundEvent
import net.minecraft.util.registry.Registry

object ItemGenerator : Generator(
    "items"
) {
    override fun generate() {
        for (item in Registry.ITEM) {
            val resourceIdentifier = Registry.ITEM.getId(item)
            val itemData = JsonObject()
            itemData.addProperty("id", Registry.ITEM.getRawId(item))
            item.group?.let {
                itemData.addProperty("category", CreativeInventoryTabGenerator.CATEGORY_ID_FIELD.getInt(it))
            }
            itemData.addProperty("rarity", item.getRarity(ItemStack.EMPTY).ordinal)
            itemData.addProperty("max_stack_size", item.maxCount)
            itemData.addProperty("max_damage", item.maxDamage)
            IS_FIRE_RESISTANT_FIELD?.let {
                itemData.addProperty("is_fire_resistant", it.getBoolean(item))
            }
            itemData.addProperty("is_complex", item.isNetworkSynced)
            item.translationKey?.let {
                itemData.addProperty("translation_key", it)
            }
            item.foodComponent?.let {
                itemData.add("food_properties", it.serialize())
            }
            // itemData.addProperty("default_destroy_speed", item.getDestroySpeed(ItemStack.EMPTY, Registry.BLOCK.get(Registry.BLOCK.defaultKey).defaultBlockState()))


            if (item is BlockItem) {
                itemData.addProperty("block", Registry.BLOCK.getRawId(item.block))
            }
            if (item is ToolItem) {
                itemData.addProperty("uses", item.material.durability)
                itemData.addProperty("speed", TOOL_ITEM_GETTER.invoke(item.material) as Float)
                itemData.addProperty("attack_damage_bonus", item.material.attackDamage)
                itemData.addProperty("level", item.material.miningLevel)
                itemData.addProperty("enchantment_value", item.material.enchantability)
                // ToDo itemData.addProperty("repair_ingredient", item.tier.repairIngredient.serialize())
            }
            if (item is MiningToolItem) {
                val blocks = JsonArray()
                (DIGGER_ITEM_BLOCKS_FIELD.get(item) as Set<Block>?)?.toSortedSet { block: Block, block1: Block -> Registry.BLOCK.getRawId(block) - Registry.BLOCK.getRawId(block1) }?.let {
                    for (block in it) {
                        blocks.add(Registry.BLOCK.getRawId(block))
                    }
                }
                if (blocks.size() > 0) {
                    itemData.add("diggable_blocks", blocks)
                }
                itemData.addProperty("speed", DIGGER_ITEM_SPEED_FIELD.getFloat(item))
                itemData.addProperty("attack_damage", DIGGER_ITEM_ATTACK_DAMAGE.getFloat(item))
            }
            if (item is ArmorItem) {
                itemData.addProperty("equipment_slot", (ARMOR_ITEM_EQUIPMENT_SLOT_FIELD.get(item) as EquipmentSlot).name.toLowerCase())
                itemData.addProperty("defense", item.protection)
                itemData.addProperty("toughness", ARMOR_ITEM_TOUGHNESS_FIELD.getFloat(item))
                itemData.addProperty("armor_material", item.material.name.toLowerCase())
                ARMOR_ITEM_KNOCKBACK_RESISTANCE?.getFloat(item)?.let {
                    itemData.addProperty("knockback_resistance", it)
                }
            }

            if (item is AxeItem) {
                val strippables = JsonArray()
                (AXE_ITEM_STRIPPABLES_FIELD.get(item) as Map<Block, Block>?)?.toSortedMap { block: Block, block1: Block -> Registry.BLOCK.getRawId(block) - Registry.BLOCK.getRawId(block1) }?.let {
                    for ((_, block) in it) {
                        strippables.add(Registry.BLOCK.getRawId(block))
                    }
                }
                if (strippables.size() > 0) {
                    itemData.add("strippables_blocks", strippables)
                }
            }
            if (item is BucketItem) {
                (BUCKED_ITEM_CONTENT_FIELD.get(item) as Fluid?)?.let {
                    itemData.addProperty("bucked_fluid_type", Registry.FLUID.getRawId(it))
                }
            }
            if (item is DyeItem) {
                itemData.addProperty("dye_color", item.color.name.toLowerCase())
            }
            if (item::class.java == MOB_BUCKED_ITEM_CLASS) {
                (MOB_BUCKED_ITEM_TYPE_FIELD?.get(item) as EntityType<*>?)?.let {
                    itemData.addProperty("bucket_fish_type", Registry.ENTITY_TYPE.getRawId(it))
                }
                (MOB_BUCKED_ITEM_EMPTY_SOUND_FIELD?.get(item) as SoundEvent?)?.let {
                    itemData.addProperty("bucket_empty_sound", Registry.SOUND_EVENT.getRawId(it))
                }
            }
            if (item is HoeItem) {
                val tillables = JsonArray()
                (HOE_ITEM_TILLABLES_FIELD.get(item) as Map<Block, BlockState>?)?.toSortedMap { block: Block, block1: Block -> Registry.BLOCK.getRawId(block) - Registry.BLOCK.getRawId(block1) }?.let {
                    for ((_, state) in it) {
                        tillables.add(Block.getRawIdFromState(state))
                    }
                }
                if (tillables.size() > 0) {
                    itemData.add("tillables_block_states", tillables)
                }
            }
            if (item is HorseArmorItem) {
                itemData.addProperty("horse_protection", item.bonus)
                itemData.addProperty("horse_texture", item.entityTexture.toString())
            }
            if (item is ShovelItem) {
                val flattenables = JsonArray()
                (SHOVEL_ITEM_FLATTENABLES_FIELD.get(item) as Map<Block, BlockState>?)?.toSortedMap { block: Block, block1: Block -> Registry.BLOCK.getRawId(block) - Registry.BLOCK.getRawId(block1) }?.let {
                    for ((_, state) in it) {
                        flattenables.add(Block.getRawIdFromState(state))
                    }
                }
                if (flattenables.size() > 0) {
                    itemData.add("flattenables_block_states", flattenables)
                }
            }
            if (item is SpawnEggItem) {
                itemData.addProperty("spawn_egg_color_1", item.getColor(0))
                itemData.addProperty("spawn_egg_color_2", item.getColor(1))
                (SPAWN_EGG_ITEM_ENTITY_TYPE_FIELD.get(item) as EntityType<*>?)?.let {
                    itemData.addProperty("spawn_egg_entity_type", Registry.ENTITY_TYPE.getRawId(it))
                }
            }
            if (item is SwordItem) {
                itemData.addProperty("attack_damage", item.attackDamage)
            }

            if (item is MusicDiscItem) {
                itemData.addProperty("analog_output", item.comparatorOutput)
                itemData.addProperty("sound", Registry.SOUND_EVENT.getRawId(item.sound))
            }


            itemData.addProperty("class", item::class.java.simpleName)


            // ToDo Item models

            data.add(resourceIdentifier.toString(), itemData)
        }
    }

    private fun FoodComponent.serialize(): JsonObject {
        val data = JsonObject()
        data.addProperty("nutrition", hunger)
        data.addProperty("saturation_modifier", saturationModifier)
        data.addProperty("is_meat", isMeat)
        data.addProperty("can_always_eat", isAlwaysEdible)
        data.addProperty("fast_food", isSnack)
        return data
    }

    private val MOB_BUCKED_ITEM_CLASS = getClass("net.minecraft.world.item.MobBucketItem", "net.minecraft.world.item.BucketItem")

    private val TOOL_ITEM_GETTER = getGetter(ToolMaterial::class.java, "getMiningSpeed", "getMiningSpeedMultiplier")!!


    private val IS_FIRE_RESISTANT_FIELD = getField(Item::class.java, "fireproof")

    private val DIGGER_ITEM_BLOCKS_FIELD = MiningToolItem::class.java.getDeclaredField("effectiveBlocks")
    private val DIGGER_ITEM_SPEED_FIELD = MiningToolItem::class.java.getDeclaredField("miningSpeed")
    private val DIGGER_ITEM_ATTACK_DAMAGE = getField(MiningToolItem::class.java, "attackDamageBaseline", "attackDamage")!!
    private val ARMOR_ITEM_EQUIPMENT_SLOT_FIELD = ArmorItem::class.java.getDeclaredField("slot")
    private val ARMOR_ITEM_TOUGHNESS_FIELD = ArmorItem::class.java.getDeclaredField("toughness")
    private val AXE_ITEM_STRIPPABLES_FIELD = getField(AxeItem::class.java, "STRIPABLES", "STRIPPABLES", "STRIPPED_BLOCKS")!!
    private val BUCKED_ITEM_CONTENT_FIELD = BucketItem::class.java.getDeclaredField("fluid")
    private val MOB_BUCKED_ITEM_TYPE_FIELD = getField(MOB_BUCKED_ITEM_CLASS, "type")
    private val MOB_BUCKED_ITEM_EMPTY_SOUND_FIELD = getField(MOB_BUCKED_ITEM_CLASS, "emptySound")
    private val HOE_ITEM_TILLABLES_FIELD = HoeItem::class.java.getDeclaredField("TILLED_BLOCKS")
    private val SHOVEL_ITEM_FLATTENABLES_FIELD = getField(ShovelItem::class.java, "PATH_STATES", "PATH_BLOCKSTATES")!!
    private val SPAWN_EGG_ITEM_ENTITY_TYPE_FIELD = SpawnEggItem::class.java.getDeclaredField("type")
    private val ARMOR_ITEM_KNOCKBACK_RESISTANCE = getField(ArmorItem::class.java, "knockbackResistance")

    init {
        DIGGER_ITEM_BLOCKS_FIELD.isAccessible = true
        DIGGER_ITEM_SPEED_FIELD.isAccessible = true
        ARMOR_ITEM_EQUIPMENT_SLOT_FIELD.isAccessible = true
        ARMOR_ITEM_TOUGHNESS_FIELD.isAccessible = true
        AXE_ITEM_STRIPPABLES_FIELD.isAccessible = true
        BUCKED_ITEM_CONTENT_FIELD.isAccessible = true
        HOE_ITEM_TILLABLES_FIELD.isAccessible = true
        SHOVEL_ITEM_FLATTENABLES_FIELD.isAccessible = true
        SPAWN_EGG_ITEM_ENTITY_TYPE_FIELD.isAccessible = true
    }
}
