package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.potion.Potion
import net.minecraft.util.registry.Registry

object PotionGenerator : Generator(
    "potions"
) {
    override fun generate() {
        for (potion in Registry.POTION) {
            val resourceIdentifier = Registry.POTION.getId(potion)
            val potionData = JsonObject()

            potionData.addProperty("id", Registry.POTION.getRawId(potion))

            (POTION_NAME_FIELD.get(potion) as String?)?.let {
                potionData.addProperty("name", it)
            }

            val effects = JsonArray()

            for (effect in potion.effects) {
                effects.add(effect.serialize())
            }
            if (effects.size() > 0) {
                potionData.add("effects", effects)
            }

            data.add(resourceIdentifier.toString(), potionData)
        }
    }

    val POTION_NAME_FIELD = getField(Potion::class.java, "baseName", "name")!!


    private fun StatusEffectInstance.serialize(): JsonObject {
        val mobEffect = JsonObject()

        effectType?.let {
            mobEffect.addProperty("effect", StatusEffectGenerator.STATUS_EFFECT_REGISTRY.getId(it).toString())
        }

        mobEffect.addProperty("duration", duration)
        mobEffect.addProperty("amplifier", amplifier)
        mobEffect.addProperty("ambient", isAmbient)
        mobEffect.addProperty("no_counter", isPermanent)
        mobEffect.addProperty("visible", shouldShowParticles())
        mobEffect.addProperty("showIcon", shouldShowIcon())



        return mobEffect
    }
}

