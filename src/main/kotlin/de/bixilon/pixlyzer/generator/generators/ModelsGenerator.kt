package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.Util
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry

object ModelsGenerator : Generator(
    "models"
) {
    val MODELS_TO_GENERATE: MutableList<String> = mutableListOf()
    override fun generate() {
        for (model in MODELS_TO_GENERATE) {
            val resourceLocation = Identifier(model)
            loadModel(resourceLocation)
        }
        for (item in Registry.ITEM) {
            val itemIdentifier = Registry.ITEM.getId(item)
            loadModel(Identifier(itemIdentifier.namespace, "item/" + itemIdentifier.path))
        }
    }

    private fun loadModel(resourceLocation: Identifier) {
        if (data.has(resourceLocation.toString())) {
            return
        }
        data.add(resourceLocation.toString(), Util.readJsonMinecraftResource("assets/${resourceLocation.namespace}/models/${resourceLocation.path}.json"))

        val modelJson = Util.readJsonMinecraftResource("assets/${resourceLocation.namespace}/models/${resourceLocation.path}.json")
        modelJson["parent"]?.let {
            if (!it.asString.startsWith("builtin/")) {
                loadModel(Identifier(it.asString))
            }
        }
        modelJson["overrides"]?.asJsonArray?.let {
            for (override in it) {
                override.asJsonObject["model"]?.let {
                    loadModel(Identifier(it.asString))
                }
            }
        }
    }

}
