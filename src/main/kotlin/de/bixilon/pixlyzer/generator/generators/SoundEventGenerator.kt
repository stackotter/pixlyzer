package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.util.registry.Registry

object SoundEventGenerator : Generator(
    "sound_events"
) {
    override fun generate() {
        for (soundEvent in Registry.SOUND_EVENT) {
            val resourceIdentifier = Registry.SOUND_EVENT.getId(soundEvent)
            val soundEventData = JsonObject()
            soundEventData.addProperty("id", Registry.SOUND_EVENT.getRawId(soundEvent))


            data.add(resourceIdentifier.toString(), soundEventData)
        }
    }
}
