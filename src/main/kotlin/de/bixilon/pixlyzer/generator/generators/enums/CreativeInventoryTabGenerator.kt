package de.bixilon.pixlyzer.generator.generators.enums

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.item.ItemGroup
import net.minecraft.util.registry.Registry
import java.lang.reflect.Field

object CreativeInventoryTabGenerator : Generator(
    "creative_inventory_tabs"
) {
    override fun generate() {
        for (tab in ItemGroup.GROUPS) {
            val itemData = JsonObject()

            tab.getLanguageId()?.let {
                itemData.addProperty("translation_key", it)
            }
            tab.name?.let {
                itemData.addProperty("recipe_folder_name", it)
            }
            itemData.addProperty("background_suffix", tab.texture)

            itemData.addProperty("can_scroll", tab.canScroll())
            itemData.addProperty("show_title", tab.showTitle())

            itemData.addProperty("is_right_aligned", tab.isSpecial)
            itemData.addProperty("is_top_row", tab.isTopRow)
            itemData.addProperty("column", tab.column)
            itemData.addProperty("icon", Registry.ITEM.getRawId(tab.createIcon().item))

            val enchantmentCategories = JsonArray()
            for (enchantmentCategory in tab.enchantments) {
                enchantmentCategories.add(enchantmentCategory.ordinal)
            }
            if (enchantmentCategories.size() > 0) {
                itemData.add("enchantment_categories", enchantmentCategories)
            }



            data.add(tab.getRawId().toString(), itemData)
        }
    }

    val CATEGORY_ID_FIELD: Field = ItemGroup::class.java.getDeclaredField("index")
    private val CATEGORY_LANGUAGE_ID_FIELD = ItemGroup::class.java.getDeclaredField("id")
    private val CATEGORY_BACKGROUND_SUFFIX_FIELD = ItemGroup::class.java.getDeclaredField("texture")
    private val CATEGORY_CAN_SCROLL_FIELD = ItemGroup::class.java.getDeclaredField("scrollbar")
    private val CATEGORY_SHOW_TITLE_FIELD = getField(ItemGroup::class.java, "renderName", "tooltip")!!

    init {
        CATEGORY_ID_FIELD.isAccessible = true
        CATEGORY_LANGUAGE_ID_FIELD.isAccessible = true
        CATEGORY_BACKGROUND_SUFFIX_FIELD.isAccessible = true
        CATEGORY_CAN_SCROLL_FIELD.isAccessible = true
    }


    private fun ItemGroup.getRawId(): Int {
        return CATEGORY_ID_FIELD.getInt(this)
    }

    private fun ItemGroup.getLanguageId(): String? {
        return CATEGORY_LANGUAGE_ID_FIELD.get(this) as String?
    }

    private fun ItemGroup.getBackgroundSuffix(): String {
        return CATEGORY_BACKGROUND_SUFFIX_FIELD.get(this) as String
    }

    private fun ItemGroup.canScroll(): Boolean {
        return CATEGORY_CAN_SCROLL_FIELD.getBoolean(this)
    }

    private fun ItemGroup.showTitle(): Boolean {
        return CATEGORY_SHOW_TITLE_FIELD.getBoolean(this)
    }


}
