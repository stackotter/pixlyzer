package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.entity.decoration.painting.PaintingMotive
import net.minecraft.util.registry.Registry

object MotiveGenerator : Generator(
    "motives"
) {
    override fun generate() {
        for (motive in MOTIVE_REGISTRY) {
            val resourceIdentifier = MOTIVE_REGISTRY.getId(motive)
            val motiveData = JsonObject()
            motiveData.addProperty("id", MOTIVE_REGISTRY.getRawId(motive))

            motiveData.addProperty("width", motive.width)
            motiveData.addProperty("height", motive.height)

            data.add(resourceIdentifier.toString(), motiveData)
        }
    }

    val MOTIVE_REGISTRY = getField(Registry::class.java, "PAINTING_MOTIVE", "MOTIVE")!!.get(null) as Registry<PaintingMotive>
}
