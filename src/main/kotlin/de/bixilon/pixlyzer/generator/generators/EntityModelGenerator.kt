package de.bixilon.pixlyzer.generator.generators

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.client.color.item.ItemColors
import net.minecraft.client.model.ModelPart
import net.minecraft.client.render.entity.EntityRenderDispatcher
import net.minecraft.client.render.entity.EntityRenderer
import net.minecraft.client.render.entity.LivingEntityRenderer
import net.minecraft.client.render.entity.model.AnimalModel
import net.minecraft.client.render.entity.model.CompositeEntityModel
import net.minecraft.client.render.entity.model.LlamaEntityModel
import net.minecraft.client.render.entity.model.RabbitEntityModel
import net.minecraft.client.render.item.ItemRenderer
import net.minecraft.client.render.model.BakedModelManager
import net.minecraft.client.texture.TextureManager
import net.minecraft.resource.ReloadableResourceManagerImpl
import net.minecraft.resource.ResourceType
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import org.objenesis.Objenesis
import org.objenesis.ObjenesisStd

object EntityModelGenerator : Generator(
    "entity_models"
) {
    private val OBJENESIS: Objenesis = ObjenesisStd()

    override fun generate() {
        val entityRenderDispatcher = OBJENESIS.newInstance(EntityRenderDispatcher::class.java)

        val resourceManager = ReloadableResourceManagerImpl(ResourceType.CLIENT_RESOURCES)
        val textureManager = TextureManager(resourceManager)
        val itemColors = ItemColors()
        val bakedModelManager = BakedModelManager(textureManager, BlockGenerator.DEFAULT_BLOCK_COLORS, 1)
        val itemRenderer = ItemRenderer(textureManager, bakedModelManager, itemColors)

        entityRenderDispatcher.renderers = mutableMapOf()
        entityRenderDispatcher.registerRenderers(itemRenderer, resourceManager)

        for (entityType in Registry.ENTITY_TYPE) {
            val resourceLocation = Registry.ENTITY_TYPE.getId(entityType)
            val renderer = entityRenderDispatcher.renderers[entityType]

            if (renderer != null) {
                val entityJson = renderer.toJson(resourceLocation)
                data.add("$resourceLocation", entityJson)
            } else {
                println("no renderer found for: $resourceLocation")
            }
        }
    }

    private fun EntityRenderer<*>.toJson(resourceLocation: Identifier): JsonObject {
        // model
        var modelJson: JsonObject? = null
        var modelType = "unsupported"
        when {
            this is LivingEntityRenderer<*, *> -> {
                when (val model = model) {
                    is AnimalModel -> {
                        modelJson = model.toJson()
                        modelType = "animal"
                    }
                    is CompositeEntityModel -> {
                        modelJson = model.toJson()
                        modelType = "composite"
                    }
                    is LlamaEntityModel -> {
                        modelJson = model.toJson()
                        modelType = "llama"
                    }
                    is RabbitEntityModel -> {
                        modelJson = model.toJson()
                        modelType = "rabbit"
                    }
                }
            }
            resourceLocation.toString() == "minecraft:player" -> {
                modelType = "player"
            }
            else -> {
                modelType = try {
                    val modelField = this::class.java.getField("model")
                    // the renderer has a model field
                    "non_living"
                } catch (exception: Exception) {
                    // the renderer doesn't have a model field
                    "no_model"
                }
            }
        }

        // json
        val entityJson = JsonObject()
        entityJson.addProperty("type", modelType)
        modelJson?.let {
            entityJson.add("model", it)
        }
        return entityJson
    }

    private fun AnimalModel<*>.toJson(): JsonObject {
        val headParts = this::class.java.getMethod("getHeadParts").invoke(this) as Iterable<ModelPart>
        val bodyParts = this::class.java.getMethod("getBodyParts").invoke(this) as Iterable<ModelPart>

        val json = JsonObject()
        json.add("head_parts", headParts.toJson())
        json.add("body_parts", bodyParts.toJson())
        return json
    }

    private fun CompositeEntityModel<*>.toJson(): JsonObject {
        val parts = this::class.java.getMethod("getParts").invoke(this) as Iterable<ModelPart>

        val json = JsonObject()
        json.add("parts", parts.toJson())
        return json
    }

    private fun LlamaEntityModel<*>.toJson(): JsonObject {
        val parts = listOf(
            head,
            leftBackLeg,
            leftChest,
            leftFrontLeg,
            rightBackLeg,
            rightChest,
            rightFrontLeg,
            torso,
        )

        val json = JsonObject()
        json.add("parts", parts.toJson())
        return json
    }

    private fun RabbitEntityModel<*>.toJson(): JsonObject {
        val parts = listOf(
            leftFoot,
            rightFoot,
            leftBackLeg,
            rightBackLeg,
            torso,
            leftFrontLeg,
            rightFrontLeg,
            head,
            rightEar,
            leftEar,
            tail,
            nose,
        )

        val json = JsonObject()
        json.add("parts", parts.toJson())
        return json
    }

    private fun ModelPart.toJson(): JsonObject {
        // rotation
        val rotation = JsonObject()
        val origin = listOf(pivotX, pivotY, pivotZ)
        rotation.add("origin", Gson().toJsonTree(origin))
        rotation.addProperty("yaw", yaw)
        rotation.addProperty("pitch", pitch)
        rotation.addProperty("roll", roll)

        // elements
        val elements = mutableListOf<JsonObject>()
        for (cuboid in cuboids) {
            val from = listOf(cuboid.minX, cuboid.minY, cuboid.minZ)
            val to = listOf(cuboid.maxX, cuboid.maxY, cuboid.maxZ)

            val element = JsonObject()
            element.add("from", Gson().toJsonTree(from))
            element.add("to", Gson().toJsonTree(to))
            elements.add(element)
        }

        // json
        val json = JsonObject()
        json.add("rotation", rotation)
        json.add("elements", Gson().toJsonTree(elements))
        return json
    }

    private fun Iterable<ModelPart>.toJson(): JsonElement {
        val partsJson = mutableListOf<JsonObject>()
        for (part in this) {
            val partJson = part.toJson()
            partsJson.add(partJson)
        }
        return Gson().toJsonTree(partsJson)
    }
}
