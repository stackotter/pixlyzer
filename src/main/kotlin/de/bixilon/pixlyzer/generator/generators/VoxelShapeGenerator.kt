package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.util.math.Box
import net.minecraft.util.shape.VoxelShape

object VoxelShapeGenerator : Generator(
    "shapes"
) {
    private val AABBS: MutableList<Box> = mutableListOf()

    override fun generate() {
        val shapes = JsonArray()
        for ((_, shape) in BlockGenerator.VOXEL_SHAPES) {
            shapes.add(shape.second.serialize())
        }
        data.add("shapes", shapes)


        val aabbs = JsonArray()
        for (aabb in AABBS) {
            aabbs.add(aabb.serialize())
        }
        data.add("aabbs", aabbs)
    }


    private val VOXEL_SHAPE_FACES_FIELD = getField(VoxelShape::class.java, "shapeCache")


    private fun VoxelShape.serialize(): JsonObject {
        val json = JsonObject()
        val faces = VOXEL_SHAPE_FACES_FIELD?.get(this) as Array<VoxelShape>?
        if (faces != null) {
            check(faces.size == 6)

            val facesArray = JsonArray()
            for (face in faces) {
                if (face === this) {
                    facesArray.add(JsonObject())
                    continue
                }
                facesArray.add(face.serialize())
            }

            val firstFace = faces[0]
            var allTheSame = true
            for (face in faces) {
                if (face == firstFace) {
                    allTheSame = false
                    break
                }
            }
            if (allTheSame) {
                // remove all faces, except first one
                for (i in 1 until facesArray.size()) {
                    facesArray.remove(i)
                }
            }
            var allEmpty = true
            for (faceJson in facesArray) {
                if (faceJson.asJsonObject.size() > 0) {
                    allEmpty = false
                }
            }
            if (allEmpty) {
                json.add("faces", JsonArray())
            } else {
                json.add("faces", facesArray)
            }
        }
        if (!isEmpty) {
            val bounds = boundingBox
            val boundsIndex = if (AABBS.contains(bounds)) {
                AABBS.indexOf(bounds)
            } else {
                val index = AABBS.size
                AABBS.add(bounds)
                index
            }
            json.addProperty("shape", boundsIndex)
        }
        return json
    }

    private fun Box.serialize(): JsonObject {
        val minX = AABB_MIN_X_FIELD.getDouble(this)
        val minY = AABB_MIN_Y_FIELD.getDouble(this)
        val minZ = AABB_MIN_Z_FIELD.getDouble(this)

        val maxX = AABB_MAX_X_FIELD.getDouble(this)
        val maxY = AABB_MAX_Y_FIELD.getDouble(this)
        val maxZ = AABB_MAX_Z_FIELD.getDouble(this)
        val json = JsonObject()

        val from = JsonArray()
        from.add(minX)
        from.add(minY)
        from.add(minZ)
        if (minX == minY && minY == minZ) {
            json.addProperty("from", minX)
        } else {
            json.add("from", from)
        }

        val to = JsonArray()
        to.add(maxX)
        to.add(maxY)
        to.add(maxZ)

        if (maxX == maxY && maxY == maxZ) {
            json.addProperty("to", maxX)
        } else {
            json.add("to", to)
        }

        return json
    }

    private val AABB_MIN_X_FIELD = getField(Box::class.java, "minX", "x1")!!
    private val AABB_MIN_Y_FIELD = getField(Box::class.java, "minY", "y1")!!
    private val AABB_MIN_Z_FIELD = getField(Box::class.java, "minZ", "z1")!!

    private val AABB_MAX_X_FIELD = getField(Box::class.java, "maxX", "x2")!!
    private val AABB_MAX_Y_FIELD = getField(Box::class.java, "maxY", "y2")!!
    private val AABB_MAX_Z_FIELD = getField(Box::class.java, "maxZ", "z2")!!

}
