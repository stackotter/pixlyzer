package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.fluid.EmptyFluid
import net.minecraft.fluid.LavaFluid
import net.minecraft.fluid.WaterFluid
import net.minecraft.util.registry.Registry

object FluidGenerator : Generator(
    "fluids"
) {
    override fun generate() {
        for (fluid in Registry.FLUID) {
            val fluidData = JsonObject()
            val resourceIdentifier = Registry.FLUID.getId(fluid)

            fluidData.addProperty("id", Registry.FLUID.getRawId(fluid))

            Registry.ITEM.getRawId(fluid.bucketItem).let {
                if (it == 0) {
                    return@let
                }
                fluidData.addProperty("bucket", it)
            }

            fluid.defaultState

            val render = JsonObject()

            when (fluid) {
                is LavaFluid -> {
                    fluidData.addProperty("drip_particle_type", Registry.PARTICLE_TYPE.getRawId(fluid.particle?.type))
                    when (fluid) {
                        is LavaFluid.Flowing -> {
                            render.addProperty("texture", "block/lava_flow")
                        }
                        is LavaFluid.Still -> {
                            render.addProperty("texture", "block/lava_still")
                        }
                        else -> {
                            TODO()
                        }
                    }
                }
                is WaterFluid -> {
                    fluidData.addProperty("drip_particle_type", Registry.PARTICLE_TYPE.getRawId(fluid.particle?.type))
                    when (fluid) {
                        is WaterFluid.Flowing -> {
                            render.addProperty("texture", "block/water_flow")
                        }
                        is WaterFluid.Still -> {
                            render.addProperty("texture", "block/water_still")
                        }
                        else -> {
                            TODO()
                        }
                    }
                }
                is EmptyFluid -> {
                }
                else -> {
                    TODO("Unknown fluid: $fluid")
                }
            }

            if (render.size() > 0) {
                fluidData.add("render", render)
            }

            data.add(resourceIdentifier.toString(), fluidData)
        }
    }

}
