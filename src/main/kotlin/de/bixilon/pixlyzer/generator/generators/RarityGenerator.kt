package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.util.Rarity

object RarityGenerator : Generator(
    "rarities"
) {
    override fun generate() {
        for (rarity in Rarity.values()) {
            val rarityData = JsonObject()
            rarityData.addProperty("name", rarity.name.toLowerCase())
            rarityData.addProperty("color", rarity.formatting.name.toLowerCase())

            data.add(rarity.ordinal.toString(), rarityData)
        }
    }
}
