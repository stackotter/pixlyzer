package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.enchantment.Enchantment
import net.minecraft.entity.EquipmentSlot
import net.minecraft.util.registry.Registry
import java.lang.reflect.Field

object EnchantmentGenerator : Generator(
    "enchantments"
) {
    override fun generate() {
        for (enchantment in Registry.ENCHANTMENT) {
            val resourceIdentifier = Registry.ENCHANTMENT.getId(enchantment)
            val enchantmentData = JsonObject()

            enchantmentData.addProperty("id", Registry.ENCHANTMENT.getRawId(enchantment))

            enchantment.translationKey?.let {
                enchantmentData.addProperty("translation_key", it)
            }
            enchantmentData.addProperty("rarity", (ENCHANTMENT_RARITY_FIELD.get(enchantment) as Enum<*>).ordinal)

            enchantment.type?.let {
                enchantmentData.addProperty("category", it.name.toLowerCase())
            }
            val equipmentSlots = JsonArray()

            for (slot in enchantment.getSlots()) {
                slot?.let {
                    equipmentSlots.add(slot.name.toLowerCase())
                }
            }
            if (equipmentSlots.size() > 0) {
                enchantmentData.add("slots", equipmentSlots)
            }
            enchantmentData.addProperty("minimum_level", ENCHANTMENT_MINIMUM_LEVEL_METHOD.invoke(enchantment) as Int)
            enchantmentData.addProperty("maximum_level", ENCHANTMENT_MAXIMUM_LEVEL_METHOD.invoke(enchantment) as Int)

            data.add(resourceIdentifier.toString(), enchantmentData)
        }
    }

    private val ENCHANTMENT_SLOTS_FIELD: Field = Enchantment::class.java.getDeclaredField("slotTypes")
    private val ENCHANTMENT_RARITY_FIELD: Field = getField(Enchantment::class.java, "rarity", "weight")!!

    private val ENCHANTMENT_MINIMUM_LEVEL_METHOD = try {
        Enchantment::class.java.getMethod("getMinimumLevel")
    } catch (exception: Exception) {
        Enchantment::class.java.getMethod("getMinLevel")
    }
    private val ENCHANTMENT_MAXIMUM_LEVEL_METHOD = try {
        Enchantment::class.java.getMethod("getMaximumLevel")
    } catch (exception: Exception) {
        Enchantment::class.java.getMethod("getMaxLevel")
    }

    init {
        ENCHANTMENT_SLOTS_FIELD.isAccessible = true
    }

    private fun Enchantment.getSlots(): Array<EquipmentSlot?> {
        return ENCHANTMENT_SLOTS_FIELD.get(this) as Array<EquipmentSlot?>
    }

}

