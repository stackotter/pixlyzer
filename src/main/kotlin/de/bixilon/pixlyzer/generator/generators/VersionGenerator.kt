package de.bixilon.pixlyzer.generator.generators

import de.bixilon.pixlyzer.generator.Generator
import net.minecraft.MinecraftVersion

object VersionGenerator : Generator(
    "version"
) {
    override fun generate() {
        val version = MinecraftVersion.create()

        data.addProperty("id", version.id)
        data.addProperty("name", version.name)
        data.addProperty("stable", version.isStable)
        data.addProperty("world_version", version.worldVersion)
        data.addProperty("protocol_version", version.protocolVersion)
        data.addProperty("pack_version", version.packVersion)
        data.addProperty("build_time", version.buildTime.toString())
        data.addProperty("release_target", version.releaseTarget)
    }
}
