package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonObject
import de.bixilon.pixlyzer.EntitySpawner
import de.bixilon.pixlyzer.PixLyzer
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityType
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.attribute.EntityAttribute
import net.minecraft.entity.attribute.EntityAttributeInstance
import net.minecraft.entity.data.TrackedData
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import java.lang.reflect.Method
import java.lang.reflect.Modifier
import java.util.*

object EntityGenerator : Generator(
    "entities"
) {
    private val entityClassMappings: MutableMap<Class<out Entity>, Identifier> = mutableMapOf()

    override fun generate() {
        for (entityType in Registry.ENTITY_TYPE) {
            val resourceIdentifier = Registry.ENTITY_TYPE.getId(entityType)
            val entityData = JsonObject()
            entityData.addProperty("id", Registry.ENTITY_TYPE.getRawId(entityType))
            entityType.translationKey?.let {
                entityData.addProperty("translation_key", it)
            }
            entityData.addProperty("serializable", entityType.isSaveable)
            entityData.addProperty("summonable", entityType.isSummonable)
            ENTITY_TYPE_FIRE_IMMUNE_FIELD?.getBoolean(entityType)?.let {
                entityData.addProperty("is_fire_immune", it)
            }

            CAN_SPAWN_FAR_AWAY_FROM_PLAYER_METHOD?.invoke(entityType)?.let {
                entityData.addProperty("can_spawn_far_from_player", it as Boolean)
            }

            entityType.lootTableId?.let {
                entityData.addProperty("loot_table", it.toString())
            }

            getEntitySize(entityType).let {
                entityData.addProperty("width", it.first)
                entityData.addProperty("height", it.second)
                entityData.addProperty("size_fixed", it.third)
            }

            val entity = EntitySpawner.summonEntity(entityType)
            val entity2 = EntitySpawner.summonEntity(entityType)
            // some entities have random values, we can and will ignore these ones


            // ToDo: entityData.addProperty("category", entityType.getEntityCategory(entity).ordinal)

            if (entity is LivingEntity && entity2 is LivingEntity) {
                val attributesData = JsonObject()
                for ((resourceLocation, attribute) in ATTRIBUTE_MAP) {
                    val attributes = LIVING_ENTITY_GET_ATTRIBUTES_METHOD.invoke(entity) ?: continue
                    val instance = BASE_ATTRIBUTE_MAP_GET_INSTANCE_METHOD.invoke(attributes, attribute) as EntityAttributeInstance? ?: continue
                    val value = ATTRIBUTE_INSTANCE_BASE_VALUE_METHOD.invoke(instance) as Double

                    val attributes2 = LIVING_ENTITY_GET_ATTRIBUTES_METHOD.invoke(entity2) ?: continue
                    val instance2 = BASE_ATTRIBUTE_MAP_GET_INSTANCE_METHOD.invoke(attributes2, attribute) as EntityAttributeInstance? ?: continue
                    val value2 = ATTRIBUTE_INSTANCE_BASE_VALUE_METHOD.invoke(instance2) as Double
                    if (value != value2) {
                        continue
                    }

                    attributesData.addProperty(resourceLocation, value)
                }
                if (attributesData.size() > 0) {
                    entityData.add("attributes", attributesData)
                }

                //       entity.getEatingSound(ItemStack.EMPTY)?.let {
                //           if (it != SoundEvents.GENERIC_EAT) {
                //               entityData.addProperty("eating_sound", Registry.SOUND_EVENT.getRawId(it))
                //           }
                //       }
            }

            val entityClass = entity::class.java

            entityData.addProperty("class", entityClass.simpleName)
            entityClassMappings[entityClass] = resourceIdentifier

            data.add(resourceIdentifier.toString(), entityData)
        }

        // resolve parents and meta data
        for (entityType in Registry.ENTITY_TYPE) {
            val entity: Entity
            try {
                entity = EntitySpawner.summonEntity(entityType)
            } catch (exception: Exception) {
                continue
            }
            val entityClass = entity::class.java

            val entityData = data[Registry.ENTITY_TYPE.getId(entityType).toString()]!!.asJsonObject

            generateMetaDataForParents(entityClass, entityData, data)
        }

        // Fix player
        data["minecraft:player"]?.asJsonObject?.let { playerJson ->
            fun mergePlayerDate(name: String) {
                data[name]?.asJsonObject?.let {
                    for ((key, value) in it.entrySet()) {
                        playerJson.add(key, value)
                    }
                    it.remove("class")
                    it.remove("parent")
                    data.remove(name)
                }
            }
            mergePlayerDate("RemotePlayer")
            mergePlayerDate("AbstractClientPlayerEntity")
            mergePlayerDate("PlayerEntity")
            playerJson.addProperty("class", "PlayerEntity")
        }
    }

    private fun generateMetaDataForParents(clazz: Class<out Entity>, entityData: JsonObject, data: JsonObject) {
        if (entityData.has("parent")) {
            return
        }
        generateMetaData(clazz).let {
            if (it.size() > 0) {
                entityData.add("meta", it)
            }
        }
        val superClass: Class<*> = clazz.superclass
        if (superClass != Object::class.java) {
            entityData.addProperty("parent", correctClassName(superClass.simpleName))

            val parentEntityName = if (entityClassMappings[superClass] == null) {
                // abstract class
                correctClassName(superClass.simpleName)
            } else {
                entityClassMappings[superClass].toString()
            }

            var parentData: JsonObject? = data[parentEntityName]?.asJsonObject

            if (parentData == null) {
                parentData = JsonObject()
                data.add(parentEntityName, parentData)
            }


            generateMetaDataForParents(superClass as Class<out Entity>, parentData, data)
        }
    }

    private fun correctClassName(className: String): String {
        return when (className) {
            "AgableMob" -> "AgeableMob"
            "class_4985" -> "StriderEntity"
            "class_4836" -> "PiglinEntity"
            else -> className
        }
    }

    private fun generateMetaData(clazz: Class<out Entity>): JsonObject {
        val json = JsonObject()

        for (field in clazz.declaredFields) {
            if (field.type != TrackedData::class.java) {
                continue
            }
            if (!Modifier.isStatic(field.modifiers)) {
                continue
            }
            field.isAccessible = true
            val dataAccessor: TrackedData<*> = field.get(null) as TrackedData<*>
            val entityClazzData = PixLyzer.ENTITY_META_DATA_MAPPING[correctClassName(clazz.simpleName)]?.asJsonObject ?: error("Can not find entity: " + correctClassName(clazz.simpleName))
            val entityData = entityClazzData["data"].asJsonObject
            val realFieldName = entityData[field.name]?.asString ?: error("Can not find meta data field ${field.name} for ${clazz.simpleName}")
            json.addProperty(realFieldName, dataAccessor.id)
        }

        return json
    }

    private val CAN_SPAWN_FAR_AWAY_FROM_PLAYER_METHOD: Method? = try {
        EntityType::class.java.getDeclaredMethod("method_20814")
    } catch (exception: Exception) {
        null
    }

    private val REGISTRY_KEY_VALUE_FIELD = getField(getClass("net.minecraft.util.registry.RegistryKey"), "value")

    private val ENTITY_TYPE_FIRE_IMMUNE_FIELD = getField(EntityType::class.java, "fireImmune")

    private val ATTRIBUTE_CLASS = getClass("net.minecraft.entity.attribute.Attributes", "net.minecraft.world.entity.ai.attributes.Attributes", "net.minecraft.world.entity.monster.SharedMonsterAttributes", "net.minecraft.entity.attribute.EntityAttributes")!!

    private val ATTRIBUTE_MAP: Map<String, EntityAttribute> = getAttributes()

    private val BASE_ATTRIBUTE_MAP_CLASS = getClass("net.minecraft.entity.attribute.AttributeContainer", "net.minecraft.entity.attribute.AbstractEntityAttributeContainer")!!


    private val BASE_ATTRIBUTE_MAP_GET_INSTANCE_METHOD = try {
        BASE_ATTRIBUTE_MAP_CLASS.getDeclaredMethod("createInstance", EntityAttribute::class.java)
    } catch (exception: Exception) {
        BASE_ATTRIBUTE_MAP_CLASS.getDeclaredMethod("getCustomInstance", EntityAttribute::class.java)
    }

    init {
        BASE_ATTRIBUTE_MAP_GET_INSTANCE_METHOD.isAccessible = true
    }

    private val LIVING_ENTITY_GET_ATTRIBUTES_METHOD = LivingEntity::class.java.getDeclaredMethod("getAttributes")

    private val ATTRIBUTE_INSTANCE_CLASS = getClass("net.minecraft.entity.attribute.EntityAttributeInstance")!!

    private val ATTRIBUTE_INSTANCE_BASE_VALUE_METHOD = ATTRIBUTE_INSTANCE_CLASS.getMethod("getBaseValue")

    private val ENTITY_DIMENSION_FIELD = getField(EntityType::class.java, "dimensions")
    private val ENTITY_DIMENSION_CLASS = getClass("net.minecraft.entity.EntityDimensions")
    private val ENTITY_DIMENSION_WIDTH_FIELD = getField(ENTITY_DIMENSION_CLASS, "width")
    private val ENTITY_DIMENSION_HEIGHT_FIELD = getField(ENTITY_DIMENSION_CLASS, "height")
    private val ENTITY_DIMENSION_FIXED_FIELD = getField(ENTITY_DIMENSION_CLASS, "fixed")


    private val ENTITY_WIDTH_FIELD = getField(EntityType::class.java, "field_17488")
    private val ENTITY_HEIGHT_FIELD = getField(EntityType::class.java, "field_17489")

    private fun getEntitySize(entityType: EntityType<*>): Triple<Float, Float, Boolean?> {
        ENTITY_DIMENSION_CLASS?.let {
            val dimension = ENTITY_DIMENSION_FIELD!!.get(entityType)

            return Triple(ENTITY_DIMENSION_WIDTH_FIELD!!.getFloat(dimension), ENTITY_DIMENSION_HEIGHT_FIELD!!.getFloat(dimension), ENTITY_DIMENSION_FIXED_FIELD!!.getBoolean(dimension))
        }
        return Triple(ENTITY_WIDTH_FIELD!!.getFloat(entityType), ENTITY_HEIGHT_FIELD!!.getFloat(entityType), null)
    }

    private fun getAttributes(): Map<String, EntityAttribute> {
        val attributeRegistryField = getField(Registry::class.java, "ATTRIBUTE", "ATTRIBUTES")
        val attributeNameField = try {
            EntityAttribute::class.java.getMethod("getId")
        } catch (exception: NoSuchMethodException) {
            null
        }
        val ret: MutableMap<String, EntityAttribute> = mutableMapOf()
        for (field in ATTRIBUTE_CLASS.declaredFields) {
            if (field.type != EntityAttribute::class.java) {
                continue
            }
            if (!Modifier.isStatic(field.modifiers)) {
                continue
            }
            val attribute = field.get(null) as EntityAttribute
            attributeNameField?.let {
                ret[it.invoke(attribute) as String] = attribute
            } ?: let {
                val registry = attributeRegistryField!!.get(null) as Registry<*>
                val method = attributeRegistryField.type.getMethod("getId", Object::class.java)
                var key = method.invoke(registry, attribute)
                if (key is Optional<*>) {
                    key = key.get()
                }
                if (key::class.java.simpleName == "RegistryKey") {
                    key = REGISTRY_KEY_VALUE_FIELD!!.get(key)
                }
                ret[key.toString()] = attribute
            }

        }
        return ret
    }

    fun getKeyFromMobEffect(attribute: EntityAttribute): String {
        for ((resourceLocation, found) in ATTRIBUTE_MAP) {
            if (found == attribute) {
                return resourceLocation
            }
        }
        TODO()
    }
}
