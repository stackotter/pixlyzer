package de.bixilon.pixlyzer.generator.generators

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import de.bixilon.pixlyzer.generator.Generator
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.getGetter
import de.bixilon.pixlyzer.util.Util
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.Blocks
import net.minecraft.block.FluidBlock
import net.minecraft.client.color.block.BlockColors
import net.minecraft.fluid.Fluid
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import net.minecraft.util.shape.VoxelShape

object BlockGenerator : Generator(
    "blocks"
) {
    val VOXEL_SHAPES: MutableMap<String, Pair<Int, VoxelShape>> = mutableMapOf()

    override fun generate() {
        val stateMap: MutableMap<Block, MutableSet<BlockState>> = mutableMapOf()

        for (state in BLOCK_STATE_REGISTRY) {
            val owner = BLOCK_STATE_OWNER_FIELD.get(state) as Block
            val blockSet = stateMap[owner] ?: mutableSetOf()
            if (blockSet.isEmpty()) {
                stateMap[owner] = blockSet
            }
            blockSet.add(state)
        }

        val waterCauldronBlock = Registry.BLOCK.get(Identifier("minecraft:water_cauldron")) as Block?

        for (block in Registry.BLOCK) {
            val resourceIdentifier = Registry.BLOCK.getId(block)
            val blockData = JsonObject()
            blockData.addProperty("id", Registry.BLOCK.getRawId(block))
            blockData.addProperty("explosion_resistance", block.blastResistance)
            blockData.addProperty("item", Registry.ITEM.getRawId(block.asItem()))
            if (block.slipperiness != 0.6f) {
                blockData.addProperty("friction", block.slipperiness)
            }
            BLOCK_SPEED_FACTOR_FIELD?.getFloat(block)?.let {
                if (it != 1.0f) {
                    blockData.addProperty("speed_factor", it)
                }
            }
            BLOCK_JUMP_FACTOR_FIELD?.getFloat(block)?.let {
                if (it != 1.0f) {
                    blockData.addProperty("jump_factor", it)
                }
            }
            blockData.addProperty("default_state", Block.getRawIdFromState(block.defaultState))
            if (block.hasDynamicBounds()) {
                blockData.addProperty("has_dynamic_shape", block.hasDynamicBounds())
            }
            try {
                blockData.addProperty("hardness", BLOCK_DESTROY_SPEED_FIELD.getFloat(block))
            } catch (exception: IllegalArgumentException) {
            }

            blockData.addProperty("class", block::class.java.simpleName)

            if (block is FluidBlock) {
                (FLUID_BLOCK_FLUID_FIELD.get(block) as Fluid).let {
                    if (FLOWABLE_FLUID_CLASS.isAssignableFrom(it::class.java)) {
                        blockData.addProperty("still_fluid", Registry.FLUID.getRawId(FLOWABLE_GET_STILL_METHOD.invoke(it) as Fluid))
                        blockData.addProperty("flow_fluid", Registry.FLUID.getRawId(FLOWABLE_GET_FLOWING_METHOD.invoke(it) as Fluid))
                    } else {
                        blockData.addProperty("fluid", Registry.FLUID.getRawId(it))
                    }
                }
            }


            val render = Util.readJsonMinecraftResource("assets/${resourceIdentifier.namespace}/blockstates/${resourceIdentifier.path}.json")

            val multipartVariants: MutableMap<MutableMap<String, MutableSet<String>>, MutableList<JsonElement>> = mutableMapOf()


            render["multipart"]?.asJsonArray?.let {
                for (condition in it) {
                    check(condition is JsonObject)
                    val properties: MutableSet<MutableMap<String, MutableSet<String>>> = mutableSetOf()

                    fun addPropertyMap(json: JsonObject) {
                        val singlePropertyMap: MutableMap<String, MutableSet<String>> = mutableMapOf()
                        for ((property, propertyValue) in json.entrySet()) {
                            val valueSet: MutableSet<String> = mutableSetOf()
                            val propertyValues = propertyValue.asString.split("|")
                            for (value in propertyValues) {
                                valueSet.add(value)
                            }
                            singlePropertyMap[property.toLowerCase()] = valueSet
                        }
                        properties.add(singlePropertyMap)
                    }

                    condition["when"]?.asJsonObject?.let letWhen@{
                        it["OR"]?.asJsonArray?.let {
                            for (or in it) {
                                addPropertyMap(or.asJsonObject)
                            }
                            return@letWhen
                        }
                        addPropertyMap(it)
                    }
                    if (properties.isEmpty()) {
                        properties.add(mutableMapOf())
                    }
                    addBlockModel(condition["apply"])
                    for (propertyMap in properties) {
                        multipartVariants.getOrPut(propertyMap, { mutableListOf() }).add(condition["apply"])
                    }
                }
            }

            val renderVariants: MutableMap<MutableMap<String, String>, JsonElement> = mutableMapOf()
            render["variants"]?.asJsonObject?.entrySet()?.let {
                for ((properties, variant) in it) {

                    addBlockModel(variant)
                    if (properties.isBlank()) {
                        renderVariants[mutableMapOf()] = variant
                        continue
                    }

                    val propertiesOut: MutableMap<String, String> = mutableMapOf()
                    for (split in properties.split(",")) {
                        val splitProperty = split.split("=")
                        propertiesOut[splitProperty[0].toLowerCase()] = splitProperty[1].toLowerCase()
                    }
                    renderVariants[propertiesOut] = variant
                }
            }

            val hasColorProperties = (TINT_PROPERTIES_METHOD?.invoke(DEFAULT_BLOCK_COLORS, block) as Set<*>?)?.size?.let { it > 0 } ?: let {
                val blockColorProviderList = BLOCK_COLORS_PROVIDERS_ID_LIST!!.get(DEFAULT_BLOCK_COLORS)
                ID_LIST_GET_METHOD.invoke(blockColorProviderList, Registry.BLOCK.getRawId(block)) != null
            }

            val states = JsonObject()


            // tints
            when (block) {
                Blocks.LARGE_FERN, Blocks.TALL_GRASS -> {
                    blockData.addProperty("tint", "minecraft:shearing_double_plant_tint")
                }
                Blocks.GRASS_BLOCK, Blocks.FERN, Blocks.GRASS, Blocks.POTTED_FERN -> {
                    blockData.addProperty("tint", "minecraft:grass_tint")
                }
                Blocks.SPRUCE_LEAVES, Blocks.BIRCH_LEAVES, Blocks.ATTACHED_MELON_STEM, Blocks.ATTACHED_PUMPKIN_STEM -> {
                    blockData.addProperty("tint_color", DEFAULT_BLOCK_COLORS.getColor(block.defaultState, null, null))
                }
                Blocks.OAK_LEAVES, Blocks.JUNGLE_LEAVES, Blocks.ACACIA_LEAVES, Blocks.DARK_OAK_LEAVES, Blocks.VINE -> {
                    blockData.addProperty("tint", "minecraft:foliage_tint")
                }
                Blocks.WATER, Blocks.BUBBLE_COLUMN, Blocks.CAULDRON, waterCauldronBlock -> {
                    blockData.addProperty("tint", "minecraft:water_tint")
                }
                Blocks.SUGAR_CANE -> {
                    blockData.addProperty("tint", "minecraft:sugar_cane_tint")
                }
                Blocks.LILY_PAD -> {
                    blockData.addProperty("tint", "minecraft:lily_pad_tint")
                }
            }


            for (state in stateMap[block]!!) {
                val stateData = JsonObject()

                if (state.luminance != 0) {
                    stateData.addProperty("luminance", state.luminance)
                }
                if (state.hasRandomTicks()) {
                    stateData.addProperty("is_randomly_ticking", state.hasRandomTicks())
                }
                if (state.hasSidedTransparency()) {
                    stateData.addProperty("has_side_transparency", state.hasSidedTransparency())
                }
                stateData.addProperty("sound_type_volume", state.soundGroup.volume)
                stateData.addProperty("sound_type_volume", state.soundGroup.pitch)
                stateData.addProperty("break_sound_type", Registry.SOUND_EVENT.getRawId(state.soundGroup.breakSound))
                stateData.addProperty("step_sound_type", Registry.SOUND_EVENT.getRawId(state.soundGroup.stepSound))
                stateData.addProperty("place_sound_type", Registry.SOUND_EVENT.getRawId(state.soundGroup.placeSound))
                stateData.addProperty("hit_sound_type", Registry.SOUND_EVENT.getRawId(state.soundGroup.hitSound))
                stateData.addProperty("fall_sound_type", Registry.SOUND_EVENT.getRawId(state.soundGroup.fallSound))


                REQUIRES_CORRECT_TOOL_FOR_DROP_FIELDS?.let {
                    stateData.addProperty("requires_correct_tool_for_drop", it.getBoolean(state))
                }

                if (!state.isOpaque) {
                    stateData.addProperty("is_opaque", state.isOpaque)
                }
                try {
                    stateData.addProperty("hardness", BLOCK_DESTROY_SPEED_FIELD.getFloat(state))
                } catch (exception: IllegalArgumentException) {
                }


                stateData.addProperty("material", MaterialGenerator.MATERIALS.inverse()[state.material].toString())


                try {
                    if (hasColorProperties) {
                        val tintColor = DEFAULT_BLOCK_COLORS.getColor(state, null, null)
                        when (blockData["tint"]?.asString) {
                            "minecraft:foliage_tint", "minecraft:lily_pad_tint", "minecraft:grass_tint" -> {
                            }
                            else -> {
                                if (tintColor != -1 && tintColor != 0) {
                                    stateData.addProperty("tint_color", tintColor)
                                }
                            }
                        }
                    }
                } catch (exception: Exception) {
                }

                val propertyData = JsonObject()

                for ((property, stateProperty) in state.entries) {
                    val propertyValue = (PROPERTY_NAME_METHOD.invoke(property) as String).toLowerCase()
                    val propertyName = stateProperty.toString().toLowerCase()
                    addJsonWithType(propertyName, propertyData, propertyValue)
                }

                if (propertyData.size() > 0) {
                    stateData.add("properties", propertyData)
                }

                for ((propertyMap, variant) in renderVariants) {
                    var valid = true
                    for ((propertyName, propertyValue) in propertyMap) {
                        if (propertyData[propertyName]?.asString != propertyValue) {
                            valid = false
                            break
                        }
                    }
                    if (valid) {
                        stateData.add("render", variant)
                    }
                }
                val multipart = JsonArray()
                for ((properties, elementList) in multipartVariants) {
                    var valid = true
                    for ((propertyName, values) in properties) {
                        if (!propertyData.has(propertyName)) {
                            valid = false
                            break
                        }
                        if (!values.contains(propertyData[propertyName].asString)) {
                            valid = false
                            break
                        }
                    }
                    if (valid) {
                        for (element in elementList) {
                            if (element is JsonArray) {
                                for (apply in element) {
                                    if (!multipart.contains(apply)) {
                                        multipart.add(apply)
                                    }
                                }
                            } else if (element is JsonObject) {
                                if (!multipart.contains(element)) {
                                    multipart.add(element)
                                }
                            }
                        }
                    }
                }

                if (multipart.size() > 0 && !stateData.has("render")) {
                    val multipartWrapper = JsonArray()
                    multipartWrapper.add(multipart)
                    stateData.add("render", multipartWrapper)
                }

                if (!block.hasDynamicBounds()) {
                    // cache
                    // ToDo: Still need this data on dynamic shapes :(
                    state.initShapeCache()

                    val cache = BLOCK_STATE_CACHE_FIELD.get(state)

                    CACHE_SOLID_RENDER_FIELD.getBoolean(cache).let {
                        if (!it) {
                            return@let
                        }
                        stateData.addProperty("solid_render", it)
                    }

                    CACHE_TRANSLUCENT_FIELD.getBoolean(cache).let {
                        if (it) {
                            return@let
                        }

                        stateData.addProperty("translucent", CACHE_TRANSLUCENT_FIELD.getBoolean(cache))
                    }

                    CACHE_LIGHT_BLOCK_FIELD.getInt(cache).let {
                        if (it == 0) {
                            return@let
                        }
                        stateData.addProperty("light_block", it)
                    }

                    LARGE_COLLISION_SHAPE_FIELD.getBoolean(cache).let {
                        if (!it) {
                            return@let
                        }
                        stateData.addProperty("large_collision_shape", it)
                    }
                    IS_COLLISION_SHAPE_FULL_BLOCK?.getBoolean(cache)?.let {
                        if (!it) {
                            return@let
                        }
                        stateData.addProperty("is_collision_shape_full_block", it)
                    }

                    (CACHE_OCCLUSION_SHAPES_FIELD.get(cache) as Array<VoxelShape>?)?.let {
                        val occlusionJson = JsonArray()
                        for (voxelShape in it) {
                            occlusionJson.add(getOrAddVoxelShape(voxelShape))
                        }
                        val firstFace = occlusionJson[0].asInt
                        var allTheSame = true
                        for (face in occlusionJson) {
                            if (face.asInt != firstFace) {
                                allTheSame = false
                                break
                            }
                        }
                        if (allTheSame) {
                            stateData.addProperty("occlusion_shapes", firstFace)
                        } else {
                            stateData.add("occlusion_shapes", occlusionJson)
                        }

                    }

                    (CACHE_COLLISION_SHAPES_FIELD?.get(cache) as VoxelShape?)?.let {
                        if (it.isEmpty) {
                            return@let
                        }
                        stateData.addProperty("collision_shapes", getOrAddVoxelShape(it))
                    }
                    (IS_FACE_STURDY?.get(cache) as BooleanArray?)?.let {
                        if (allTheSame(it)) {
                            stateData.addProperty("is_sturdy", it[0])
                            return@let
                        }

                        val sturdy = JsonArray()

                        for (bool in it) {
                            sturdy.add(bool)
                        }
                        stateData.add("is_sturdy", sturdy)
                    }
                }

                states.add(Block.getRawIdFromState(state).toString(), stateData)
            }

            blockData.add("states", states)



            data.add(resourceIdentifier.toString(), blockData)
        }
    }

    private val ID_LIST_CLASS = getClass("net.minecraft.util.IdList", "net.minecraft.util.collection.IdList")!!

    private val ID_LIST_GET_METHOD = ID_LIST_CLASS.getDeclaredMethod("get", Int::class.java)

    private val BLOCK_COLORS_PROVIDERS_ID_LIST = getField(BlockColors::class.java, "providers")

    private val TINT_PROPERTIES_METHOD = try {
        BlockColors::class.java.getDeclaredMethod("method_21592")
    } catch (exception: Exception) {
        null
    }


    private val PROPERTY_METHOD = getClass("net.minecraft.state.property.Property")!!

    private val PROPERTY_NAME_METHOD = PROPERTY_METHOD.getDeclaredMethod("getName")

    private fun getOrAddVoxelShape(voxelShape: VoxelShape): Int {
        val string = voxelShape.toString()
        VOXEL_SHAPES[string]?.let {
            return it.first
        }

        val pair = Pair(VOXEL_SHAPES.size, voxelShape)

        VOXEL_SHAPES[string] = pair
        return pair.first
    }

    private fun allTheSame(array: BooleanArray): Boolean {
        val firstValue = array[0]
        for (value in array) {
            if (value != firstValue) {
                return false
            }
        }
        return true
    }

    private val BLOCK_STATE_BASE_CLASS = getClass("net.minecraft.block.AbstractBlock\$AbstractBlockState", "net.minecraft.world.level.block.state.BlockBehaviour\$BlockStateBase", "net.minecraft.world.level.block.state.BlockState", "net.minecraft.block.BlockState")!!
    private val BLOCK_DESTROY_SPEED_FIELD = getField(getClass("net.minecraft.block.AbstractBlock\$Settings"), "hardness") ?: getField(BLOCK_STATE_BASE_CLASS, "destroySpeed") ?: getField(Block::class.java, "hardness")!!
    private val BLOCK_STATE_OWNER_FIELD = getField(getClass("net.minecraft.state.AbstractPropertyContainer"), "owner") ?: getField(getClass("net.minecraft.state.State"), "owner") ?: getField(getClass("net.minecraft.state.AbstractState"), "owner") ?: getField(getClass("net.minecraft.world.level.block.state.AbstractStateHolder"), "owner")!!
    private val BLOCK_STATE_CACHE_FIELD = BLOCK_STATE_BASE_CLASS.getDeclaredField("shapeCache")

    private val REQUIRES_CORRECT_TOOL_FOR_DROP_FIELDS = getField(BlockState::class.java, "requiresCorrectToolForDrops")

    private lateinit var BLOCK_STATE_CACHE_CLASS: Class<*>

    init {
        for (clazz in BLOCK_STATE_BASE_CLASS.declaredClasses) {
            if (clazz.name == "net.minecraft.block.AbstractBlock\$AbstractBlockState\$ShapeCache" || clazz.name == "net.minecraft.world.level.block.state.BlockBehaviour\$BlockStateBase\$Cache" || clazz.name == "net.minecraft.world.level.block.state.BlockState\$Cache" || clazz.name == "net.minecraft.block.BlockState\$ShapeCache") {
                BLOCK_STATE_CACHE_CLASS = clazz
                break
            }
        }
    }

    private val FLOWABLE_FLUID_CLASS = getClass("net.minecraft.fluid.BaseFluid", "net.minecraft.fluid.FlowableFluid")!!

    private val FLOWABLE_GET_STILL_METHOD = getGetter(FLOWABLE_FLUID_CLASS, "getStill")!!
    private val FLOWABLE_GET_FLOWING_METHOD = getGetter(FLOWABLE_FLUID_CLASS, "getFlowing")!!

    private val FLUID_BLOCK_FLUID_FIELD = getField(FluidBlock::class.java, "fluid")!!
    private val CACHE_SOLID_RENDER_FIELD = BLOCK_STATE_CACHE_CLASS.getDeclaredField("fullOpaque")
    private val CACHE_TRANSLUCENT_FIELD = BLOCK_STATE_CACHE_CLASS.getDeclaredField("translucent")
    private val CACHE_LIGHT_BLOCK_FIELD = BLOCK_STATE_CACHE_CLASS.getDeclaredField("lightSubtracted")
    private val CACHE_OCCLUSION_SHAPES_FIELD = getField(BLOCK_STATE_CACHE_CLASS, "extrudedFaces", "shapes")!!
    private val CACHE_COLLISION_SHAPES_FIELD = getField(BLOCK_STATE_CACHE_CLASS, "collisionShape") // ToDo: Not working in < 1.14.3
    private val LARGE_COLLISION_SHAPE_FIELD = getField(BLOCK_STATE_CACHE_CLASS, "exceedsCube", "field_17651")!!
    private val IS_FACE_STURDY = getField(BLOCK_STATE_CACHE_CLASS, "faceSturdy", "isFaceSturdy", "solidFullSquare")
    private val IS_COLLISION_SHAPE_FULL_BLOCK = getField(BLOCK_STATE_CACHE_CLASS, "exceedsCube", "shapeIsFullCube", "field_17651")


    private val BLOCK_SPEED_FACTOR_FIELD = getField(Block::class.java, "speedFactor")
    private val BLOCK_JUMP_FACTOR_FIELD = getField(Block::class.java, "jumpFactor")


    init {
        BLOCK_DESTROY_SPEED_FIELD.isAccessible = true
        BLOCK_STATE_CACHE_FIELD.isAccessible = true

        CACHE_SOLID_RENDER_FIELD.isAccessible = true
        CACHE_TRANSLUCENT_FIELD.isAccessible = true
        CACHE_LIGHT_BLOCK_FIELD.isAccessible = true
        CACHE_OCCLUSION_SHAPES_FIELD.isAccessible = true
        LARGE_COLLISION_SHAPE_FIELD.isAccessible = true
    }

    private val BLOCK_STATE_REGISTRY: Iterable<BlockState> = Block::class.java.getDeclaredField("STATE_IDS").get(null) as Iterable<BlockState>


    val DEFAULT_BLOCK_COLORS = BlockColors.create()


    fun addJsonWithType(input: String, output: JsonObject, name: String) {
        try {
            output.addProperty(name, Integer.parseInt(input))
            return
        } catch (exception: Exception) {
        }

        try {
            val boolean = when (input) {
                "true" -> true
                "false" -> false
                else -> throw Exception("Not a boolean")
            }
            output.addProperty(name, boolean)
            return
        } catch (exception: Exception) {
        }

        output.addProperty(name, input)
    }

    private fun addBlockModel(json: JsonElement) {
        when (json) {
            is JsonObject -> {
                ModelsGenerator.MODELS_TO_GENERATE.add(json["model"].asString)
            }
            is JsonArray -> {
                for (element in json) {
                    ModelsGenerator.MODELS_TO_GENERATE.add(element.asJsonObject["model"].asString)
                }
            }
            else -> {
                error("Invalid variant json")
            }
        }
    }
}

