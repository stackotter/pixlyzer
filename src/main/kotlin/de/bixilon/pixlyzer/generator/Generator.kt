package de.bixilon.pixlyzer.generator

import com.google.gson.JsonObject

abstract class Generator(
    val name: String,
    val allowedFail: Boolean = false,
    val allowEmpty: Boolean = false,
) {
    val entries
        get() = data.size()

    val data = JsonObject()

    abstract fun generate()

    open fun test() {}
}
