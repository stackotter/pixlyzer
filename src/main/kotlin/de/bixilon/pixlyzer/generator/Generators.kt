package de.bixilon.pixlyzer.generator

import de.bixilon.pixlyzer.generator.generators.*
import de.bixilon.pixlyzer.generator.generators.enums.BiomePrecipitationsGenerator
import de.bixilon.pixlyzer.generator.generators.enums.CreativeInventoryTabGenerator
import de.bixilon.pixlyzer.generator.generators.enums.EnchantmentTargetsGenerator


object Generators {
    val GENERATORS: List<Generator> = mutableListOf(
        EntityModelGenerator,
        EntityGenerator,
        BiomeGenerator,
        MaterialGenerator,
        ItemGenerator,
        StatusEffectGenerator,
        EnchantmentGenerator,
        PotionGenerator,
        MotiveGenerator,
        DimensionGenerator,
        FluidGenerator,
        BlockGenerator,
        RarityGenerator,
        // ToDo: MobCategoryGenerator,
        ParticleGenerator,
        BlockEntityGenerator,
        StatisticsGenerator,
        VersionGenerator,
        VillagerTypeGenerator,
        PointOfInterestGenerator,
        VillagerProfessionGenerator,
        // ToDo:    MenuTypeGenerator,
        SoundEventGenerator,
        BiomeCategoryGenerator,
        BiomePrecipitationsGenerator,

        EnchantmentTargetsGenerator,
        CreativeInventoryTabGenerator,


        VoxelShapeGenerator,
        ModelsGenerator,
    )
}
