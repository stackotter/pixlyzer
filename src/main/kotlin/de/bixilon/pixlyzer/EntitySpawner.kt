package de.bixilon.pixlyzer

import com.mojang.authlib.GameProfile
import de.bixilon.pixlyzer.util.ReflectionUtil.getClass
import de.bixilon.pixlyzer.util.ReflectionUtil.getField
import de.bixilon.pixlyzer.util.ReflectionUtil.setFinalField
import net.minecraft.client.network.OtherClientPlayerEntity
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityType
import net.minecraft.entity.boss.WitherEntity
import net.minecraft.entity.decoration.ItemFrameEntity
import net.minecraft.scoreboard.Scoreboard
import net.minecraft.world.World
import net.minecraft.world.border.WorldBorder
import org.objenesis.Objenesis
import org.objenesis.ObjenesisStd
import java.util.*


object EntitySpawner {
    private val EGG_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.ThrownEggEntity", "net.minecraft.entity.projectile.thrown.EggEntity")!!
    private val SNOWBAL_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.SnowballEntity", "net.minecraft.entity.projectile.thrown.SnowballEntity")!!
    private val ENDER_PEARL_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.ThrownEnderpearlEntity", "net.minecraft.entity.projectile.thrown.EnderPearlEntity")!!
    private val EYE_OF_ENDER_ENTITY_CLASS = getClass("net.minecraft.entity.EnderEyeEntity", "net.minecraft.entity.EyeOfEnderEntity", "net.minecraft.entity.projectile.thrown.EyeOfEnderEntity")!!
    private val EXPERIENCE_BOTTLE_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.ThrownExperienceBottleEntity", "net.minecraft.entity.projectile.thrown.ExperienceBottleEntity")!!
    private val POTION_ENTITY_ENTITY_CLASS = getClass("net.minecraft.entity.thrown.ThrownPotionEntity", "net.minecraft.entity.projectile.thrown.PotionEntity")!!

    fun summonEntity(entityType: EntityType<*>): Entity {
        when (entityType) {
            EntityType.PLAYER -> return OtherClientPlayerEntity::class.java.getConstructor(levelClass, GameProfile::class.java).newInstance(CLIENT_LEVEL, GameProfile(UUID.randomUUID(), "dummy"))
            EntityType.LIGHTNING_BOLT -> return OBJENSIS.newInstance(LIGHTNING_BOLT_CLASS) as Entity
            EntityType.FISHING_BOBBER -> return OBJENSIS.newInstance(FISHING_HOOK_CLASS) as Entity
            EntityType.ITEM_FRAME -> return OBJENSIS.newInstance(ItemFrameEntity::class.java) as Entity
        }


        val entity = try {
            ENTITY_CREATE_METHOD?.invoke(FACTORY_FIELD.get(entityType), entityType, CLIENT_LEVEL) as Entity?
        } catch (exception: Throwable) {
            exception.printStackTrace()
            null // ToDo
        }


        if (entity != null) {
            return entity
        }

        // ToDo: This crashes in 21w13a, is an issue in tiny remapper, should be fixed by now
        when (entityType) {
            EntityType.EGG -> return OBJENSIS.newInstance(EGG_ENTITY_CLASS) as Entity
            EntityType.SNOWBALL -> return OBJENSIS.newInstance(SNOWBAL_ENTITY_CLASS) as Entity
            EntityType.ENDER_PEARL -> return OBJENSIS.newInstance(ENDER_PEARL_ENTITY_CLASS) as Entity
            EntityType.EYE_OF_ENDER -> return OBJENSIS.newInstance(EYE_OF_ENDER_ENTITY_CLASS) as Entity
            EntityType.EXPERIENCE_BOTTLE -> return OBJENSIS.newInstance(EXPERIENCE_BOTTLE_ENTITY_CLASS) as Entity
            EntityType.POTION -> return OBJENSIS.newInstance(POTION_ENTITY_ENTITY_CLASS) as Entity
            EntityType.WITHER -> return OBJENSIS.newInstance(WitherEntity::class.java) as Entity
        }

        TODO("Entity type: $entityType")
    }


    private val LIGHTNING_BOLT_CLASS = getClass("net.minecraft.entity.LightningEntity")!!
    private val FISHING_HOOK_CLASS = getClass("net.minecraft.entity.FishingBobberEntity", "net.minecraft.entity.projectile.FishingBobberEntity")!!

    private val levelClass = getClass("net.minecraft.client.world.ClientWorld")

    private val FACTORY_FIELD = getField(EntityType::class.java, "factory")!!

    private val ENTITY_CREATE_METHOD = try {
        getClass("net.minecraft.entity.EntityType\$EntityFactory")?.getDeclaredMethod("create", EntityType::class.java, World::class.java)
    } catch (exception: Exception) {
        null
    }

    private val LEGACY_ENTITY_CREATE_METHOD = try {
        getClass("net.minecraft.entity.EntityType\$EntityFactory")?.getDeclaredMethod("create", EntityType::class.java, World::class.java)
    } catch (exception: Exception) {
        null
    }

    var OBJENSIS: Objenesis = ObjenesisStd()

    private val CLIENT_LEVEL = OBJENSIS.newInstance(levelClass) as World


    init {
        setFinalField(getField(World::class.java, "random")!!, CLIENT_LEVEL, Random())
        setFinalField(getField(World::class.java, "border")!!, CLIENT_LEVEL, WorldBorder())
        setFinalField(getField(World::class.java, "properties")!!, CLIENT_LEVEL, OBJENSIS.newInstance(getClass("net.minecraft.world.level.LevelProperties")))
        setFinalField(getField(levelClass, "scoreboard")!!, CLIENT_LEVEL, Scoreboard())
        getClass("net.minecraft.world.dimension.OverworldDimension")?.let {
            setFinalField(getField(World::class.java, "dimension")!!, CLIENT_LEVEL, OBJENSIS.newInstance(it))
        }
    }

}
