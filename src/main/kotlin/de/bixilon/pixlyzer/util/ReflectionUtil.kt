package de.bixilon.pixlyzer.util

import org.apache.commons.lang3.reflect.FieldUtils
import java.lang.reflect.Field
import java.lang.reflect.Method

object ReflectionUtil {

    fun getClass(vararg names: String): Class<*>? {
        for (name in names) {
            try {
                return Class.forName(name)
            } catch (exception: ClassNotFoundException) {

            }
        }
        return null
    }

    fun getField(clazz: Class<*>?, vararg names: String): Field? {
        if (clazz == null) {
            return null
        }
        for (name in names) {
            try {
                val field = clazz.getDeclaredField(name)
                field.isAccessible = true
                return field
            } catch (exception: NoSuchFieldException) {
                continue
            }
        }
        return null
    }

    fun getGetter(clazz: Class<*>?, vararg methodNames: String): Method? {
        if (clazz == null) {
            return null
        }
        for (name in methodNames) {
            try {
                val method = clazz.getMethod(name)
                method.isAccessible = true
                return method
            } catch (exception: NoSuchMethodException) {
                continue
            }
        }
        return null
    }

    fun setFinalField(field: Field, instance: Any, value: Any) {
        field.isAccessible = true

        FieldUtils.removeFinalModifier(field)

        field.set(instance, value)
    }
}
