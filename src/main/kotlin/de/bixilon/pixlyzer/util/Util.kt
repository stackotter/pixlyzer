package de.bixilon.pixlyzer.util

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import net.minecraft.client.MinecraftClient
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStreamReader

object Util {
    fun <T> forceClassInit(clazz: Class<T>) {
        try {
            Class.forName(clazz.name, true, clazz.classLoader)
        } catch (exception: ClassNotFoundException) {
            throw RuntimeException(exception)
        }
    }

    fun readJsonResource(path: String): JsonObject {
        val reader = InputStreamReader(Util::class.java.getResourceAsStream("/$path"))
        val json: JsonObject = JsonParser().parse(reader).asJsonObject
        reader.close()
        return json
    }

    fun readJsonFile(path: String): JsonObject {
        val reader = InputStreamReader(FileInputStream(path))
        val json: JsonObject = JsonParser().parse(reader).asJsonObject
        reader.close()
        return json
    }

    fun byteArrayToHexString(byteArray: ByteArray): String {
        val result = StringBuilder()
        for (value in byteArray) {
            result.append(((value.toInt() and 0xff) + 0x100).toString(16).substring(1))
        }
        return result.toString()
    }


    fun readJsonMinecraftResource(path: String): JsonObject {
        val inputStream = MinecraftClient::class.java.getResourceAsStream("/$path") ?: throw FileNotFoundException("Can not find minecraft resource: $path")
        val reader = InputStreamReader(inputStream)
        val json: JsonObject = JsonParser().parse(reader).asJsonObject
        reader.close()
        return json
    }

}
