import os
import shutil
import subprocess
import urllib.request
import zipfile
from datetime import datetime
from pathlib import Path

import ujson

# ToDo: "19w11b", "19w11a", "19w09a", "19w08b", "19w08a", "19w07a", "19w06a", "19w05a", "19w04b", "19w04a", "19w03c", "19w03b", "19w03a", "19w02a", "18w50a", "18w49a", "18w48b" "18w48a", "18w47b", "18w47a", "18w46a", "18w45a", "18w44a", "18w43c", "18w43b"

DOWNLOAD_UNTIL_VERSION = "1.16.1"
SKIP_VERSIONS = ["1.14.2 Pre-Release 1"]

HASH_FOLDER = os.path.abspath("data/hash/") + "/"
ASSETS_HASH_INDEX_FILE = os.path.abspath("data/index")
OUT_FOLDER = os.path.abspath("data/version/") + "/"
DATA_FOLDER = os.path.abspath("data/data/") + "/"

if not os.path.isdir(DATA_FOLDER):
    os.mkdir(DATA_FOLDER)

JAVA_PATH = "java"
TINY_REMAPPER_DOWNLOAD_PATH = "https://files.player.to/tmp/tiny-remapper-0.3.1-fat-player.jar"
TINY_REMAPPER_PATH = DATA_FOLDER + "tiny-remapper-0.3.1-fat-player.jar"

OBJENSIS_PATH = DATA_FOLDER + "objenesis-tck-3.1.jar"
OBJENSIS_DOWNLOAD_PATH = "https://repo1.maven.org/maven2/org/objenesis/objenesis/3.1/objenesis-3.1.jar"

KOTLIN_STD_LIB_PATH = DATA_FOLDER + "kotlin-stdlib-1.4.30.jar"
KOTLIN_STD_LIB_DOWNLOAD_PATH = "https://repo1.maven.org/maven2/org/jetbrains/kotlin/kotlin-stdlib/1.4.32/kotlin-stdlib-1.4.32.jar"
ADDITIONAL_CLASSPATH = KOTLIN_STD_LIB_PATH + ":" + OBJENSIS_PATH

VERSION_MANIFEST_URL = "https://launchermeta.mojang.com/mc/game/version_manifest.json"
YARN_MANIFEST_URL = "https://maven.fabricmc.net/net/fabricmc/yarn/versions.json"

TINY_MAPPINGS_BASE_URL = "https://maven.fabricmc.net/net/fabricmc/yarn/"
INTERMEDIARY_BASE_URL = "https://maven.fabricmc.net/net/fabricmc/intermediary/"

COMPILE_VERSION = "1.16.1"

failedVersionIds = []
partlyFailedVersionIds = []

print("Starting PixLyzer-yarn generator")
print("Downloading version manifest")
VERSION_MANIFEST = ujson.loads(urllib.request.urlopen(VERSION_MANIFEST_URL).read().decode("utf-8"))

YARN_MANIFEST = ujson.loads(urllib.request.urlopen(YARN_MANIFEST_URL).read().decode("utf-8"))


def searchVersion(versionId):
    global VERSION_MANIFEST
    for versionEntry in VERSION_MANIFEST["versions"]:
        if versionEntry["id"] == versionId:
            return versionEntry
    raise Exception("Unknown version: %s" % versionId)


startWithVersion = input("Enter version to start with: ")
if startWithVersion == "":
    startWithVersion = VERSION_MANIFEST["versions"][0]["id"]
    print("No version provided, starting with %s" % startWithVersion)
searchVersion(startWithVersion)


def runAndWait(command):
    process = subprocess.Popen(command, cwd=r'../', shell=True)
    exitCode = process.wait()
    if exitCode != 0:
        print(process.stdout.read().decode('utf-8'))
        print(process.stderr.read().decode('utf-8'))
    return exitCode


def getVersionJson(versionEntry):
    # check cache
    global DATA_FOLDER
    versionId = versionEntry["id"]
    path = DATA_FOLDER + versionId + "_yarn/" + versionId + ".json"
    if not os.path.isfile(path):
        # download
        print("Debug: Downloading %s.json" % versionEntry["id"])
        Path(path).parent.mkdir(parents=True, exist_ok=True)
        urllib.request.urlretrieve(versionEntry["url"], path)

    return ujson.load(open(path))


def checkFile(filename, url, versionId):
    if not os.path.isfile(filename):
        print("Downloading %s for  %s" % (filename, versionId))
        urllib.request.urlretrieve(url.replace(" ", "%20"), filename + ".tmp")
        os.rename(filename + ".tmp", filename)


def checkServerJar(jarBasePath, version, versionJson):
    jarPath = jarBasePath + ".jar"

    if not os.path.isfile(jarPath):
        mojangJarPath = jarBasePath + "_server_mojang.jar"
        checkFile(mojangJarPath, versionJson["downloads"]["server"]["url"], version["id"])


def checkTinyMappings(path, url, version):
    if os.path.isfile(path):
        return
    checkFile(path + ".jar", url, version)

    with zipfile.ZipFile(path + ".jar") as zipFile:
        with zipFile.open('mappings/mappings.tiny') as mappingsEntry, open(path, 'wb') as outFile:
            shutil.copyfileobj(mappingsEntry, outFile)
    os.remove(path + ".jar")


def mapJar(inputFile, outputFile, mappings, mapFrom, mapTo):
    runAndWait(
        JAVA_PATH + " -jar " + TINY_REMAPPER_PATH +
        " \"" + inputFile + "\"" +
        " \"" + outputFile + "\"" +
        " \"" + mappings + "\"" +
        " \"" + mapFrom + "\"" +
        " \"" + mapTo + "\"" +
        " --renameinvalidlocals --rebuildsourcefilenames --resolvemissing --fixpackageaccess --checkpackageaccess"
    )
    print("Done")


def checkDeobfuscatedClientJar(jarBasePath, version, versionJson):
    exhibitionismJarPath = jarBasePath + "-exhibitionism.jar"
    if not os.path.isfile(exhibitionismJarPath):
        yarnJarPath = jarBasePath + "-named.jar"
        if not os.path.isfile(yarnJarPath):
            clientJarPath = jarBasePath + "_client.jar"

            intermediaryMappedJarPath = jarBasePath + "_intermediary.jar"
            if not os.path.isfile(intermediaryMappedJarPath):
                checkFile(clientJarPath, versionJson["downloads"]["client"]["url"], version["id"])
                intermediaryMappingsPath = jarBasePath + "_intermediary.tiny"
                checkTinyMappings(intermediaryMappingsPath, INTERMEDIARY_BASE_URL + version["id"] + "/intermediary-" + version["id"] + "-v2.jar", version["id"])

                print("Mapping intermediary jar for %s" % (version["id"]))
                mapJar(
                    inputFile=clientJarPath,
                    outputFile=intermediaryMappedJarPath,
                    mappings=intermediaryMappingsPath,
                    mapFrom="official",
                    mapTo="intermediary",
                )
                os.remove(clientJarPath)
                os.remove(intermediaryMappingsPath)

            yarnMappingsPath = jarBasePath + "_yarn.tiny"
            buildName = version["id"] + "+build." + str(YARN_MANIFEST[version["id"]][-1])
            checkTinyMappings(yarnMappingsPath, TINY_MAPPINGS_BASE_URL + buildName + "/yarn-" + buildName + "-v2.jar", version["id"])

            print("Mapping named jar for %s" % (version["id"]))
            mapJar(
                inputFile=intermediaryMappedJarPath,
                outputFile=yarnJarPath,
                mappings=yarnMappingsPath,
                mapFrom="intermediary",
                mapTo="named",
            )

            os.remove(intermediaryMappedJarPath)
            os.remove(yarnMappingsPath)

        if not os.path.isfile(jarBasePath + "-exhibitionism.jar"):
            if not os.path.isfile("exhibitionism.jar"):
                raise Exception("Can not find exhibitionism.jar!")

            runAndWait("java -cp \"./wrapper/exhibitionism.jar\" kr.heartpattern.exhibitionism.AppKt --input \"%s\" --output \"%s\" --parallel 8" % (yarnJarPath, exhibitionismJarPath + ".tmp"))
            os.rename(exhibitionismJarPath + ".tmp", exhibitionismJarPath)
            os.remove(yarnJarPath)


def checkJars(version, versionJson):
    global DATA_FOLDER
    jarBasePath = DATA_FOLDER + version["id"] + "_yarn/" + version["id"]
    checkDeobfuscatedClientJar(jarBasePath, version, versionJson)
    checkServerJar(jarBasePath, version, versionJson)


def replaceInFile(file, search, replace):
    with open(file, "rt") as fin:
        with open(file, "wt") as fout:
            for line in fin:
                fout.write(line.replace(search, replace))


def compilePixLyzer():
    print("Compiling PixLyzer...")
    compileVersion = {}
    # TODO WTF
    searched = searchVersion(COMPILE_VERSION)
    for versionEntry in searched:
        compileVersion[versionEntry] = searched[versionEntry]

    checkJars(compileVersion, getVersionJson(compileVersion))
    if runAndWait('mvn clean verify') != 0:
        raise Exception("Can not compile PixLyzer")
    print("PixLyzer is now compiled")


print("Checking libraries...")

checkFile(filename=TINY_REMAPPER_PATH, url=TINY_REMAPPER_DOWNLOAD_PATH, versionId="libraries")
checkFile(filename=OBJENSIS_PATH, url=OBJENSIS_DOWNLOAD_PATH, versionId="libraries")
checkFile(filename=KOTLIN_STD_LIB_PATH, url=KOTLIN_STD_LIB_DOWNLOAD_PATH, versionId="libraries")

compilePixLyzer()

startWorking = False

for version in VERSION_MANIFEST["versions"]:
    if version["id"] == startWithVersion:
        startWorking = True

    if not startWorking:
        continue

    if version["id"] in SKIP_VERSIONS:
        print("Skipping %s" % version["id"])
        continue

    if version["id"] == DOWNLOAD_UNTIL_VERSION:
        print("Breaking at %s" % version["id"])
        break

    versionJson = getVersionJson(version)

    checkJars(version, versionJson)

    versionStartTime = datetime.now()
    print("Generating data for %s" % version["id"])
    # execute
    if runAndWait("%s -Xverify:none -classpath \"%s:%s:%s:%s\" de.bixilon.pixlyzer.PixLyzer \"%s\" \"%s\" \"%s\" \"%s\"" % (JAVA_PATH, ADDITIONAL_CLASSPATH, DATA_FOLDER + version["id"] + "_yarn/" + version["id"] + "-exhibitionism.jar", DATA_FOLDER + version["id"] + "_yarn/" + version["id"] + "_server_mojang.jar", "target/classes", OUT_FOLDER + version["id"], HASH_FOLDER, ASSETS_HASH_INDEX_FILE, version["id"])) != 0:
        print("PixLyzer did not run successfully for %s" % version["id"])
        break

    print("Done generating %s in %s" % (version["id"], (datetime.now() - versionStartTime)))

print("Done with downloading and generating all data")
