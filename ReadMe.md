# PixLyzer

This tool acts like a minecraft jar mod. It generates all kind of data for versions that are supported by yarn. This is just the repository for the program, if you search for data, check here:
https://gitlab.bixilon.de/bixilon/pixlyzer-data

## Contributing

I'd be happy. Just make a PR and ensure, it is working for most versions (And please test with: `latest`, `1.16.5`, `1.16.2`, `1.15.2`, `1.14.4`). Also tell me, if I should regenerate the assets for older versions. If you are not sure what branch to choose: `yarn`. The `mojang` is old and won't be used anymore.


## Generated data

- All ids
- Entity data
  - Entity id, sizes, health, modifiers
  - Identifier, Inherits
  - Meta data
  - More
- Items
  - Diggable blocks
  - Spawn eggs tint colors
    - Dig speeds
    - Valid slots (for armor)
    - Way more!
- Dimensions
- Blocks
    - Models
    - States
    - Explosion resistance
    - Hardness
    - Sound effects
    - Tint colors (for redstone, etc)
    - Speed and Jump modifiers
    - Way more!
- Biomes
    - Tint colors
    - Temperature, Scale, Downfall, etc
    - Sky, fog color, etc
- Biome categories
- Biome precipitations
- Block entities
- Creative inventory tabs
- Enchantments
    - Min/Max Level
    - Categories
- Rarities
- Menu Types (aka inventories)
- Mob effects
    - Modifiers
- Motives
    - Sizes
- Sounds
- Villager data
    - Professions
    - Points of interest
    - Types
- Particles
  - Models
- Statistics
- General version information
  - Version name
  - Protocol version
  - Data version
- Block entities
- Sounds
  - Categories
  - Sounds
- Way more!

## Planned things

- Entity models
- Entity events/animation/states (however you want to call them)
- Block entity models
- Packets
- Way more!

## Dependencies

- [Maven](https://maven.apache.org/)
- [yarn](https://github.com/FabricMC/yarn)
- [objenesis](https://github.com/easymock/objenesis)
- [Kotlin](https://kotlinlang.org/)
- [Exhibitionism](https://github.com/HeartPattern/Exhibitionism)
- [ujson](https://pypi.org/project/ujson/)
